import os
from array import array
import math

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from Base.Modules.baseModules import JetLepMetSyst
from analysis_tools.utils import import_root

ROOT = import_root()

class HttFitRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HttFitRDFProducer, self).__init__(*args, **kwargs)
        self.isZZAnalysis = kwargs.pop("isZZAnalysis", False)
        self.bypass_HttFit = kwargs.pop("bypass_HttFit", False)
        self.algo = kwargs.pop("algo", "FastMTT")

        if not os.getenv("_HTTFIT"):
            os.environ["_HTTFIT"] = "httfit"

            # Import libToolsTools library
            if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libToolsTools.so")
            base = "{}/{}/src/Tools/Tools".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))

            # Import the interface (SVFit or FastMTT)
            ROOT.gROOT.ProcessLine(".L {}/interface/{}Interface.h".format(base,self.algo))

            # Declare the algo
            ROOT.gInterpreter.Declare("auto httfit = {}Interface();".format(self.algo))

            # Declare compute_httfit function and set inputs
            ROOT.gInterpreter.Declare("""
                using Vfloat = const ROOT::RVec<float>&;
                using Vint = const ROOT::RVec<int>&;

                ROOT::RVec<double> compute_httfit(bool bypass_HttFit,
                        int pairType, int dau1_index, int dau2_index,
                        Vfloat muon_pt, Vfloat muon_eta, Vfloat muon_phi, Vfloat muon_mass,
                        Vfloat electron_pt, Vfloat electron_eta, Vfloat electron_phi, Vfloat electron_mass,
                        Vfloat tau_pt, Vfloat tau_eta, Vfloat tau_phi, Vfloat tau_mass, Vint Tau_decayMode,
                        float met_pt, float met_phi, float met_covXX, float met_covXY, float met_covYY) {

                    if (pairType < 0 || bypass_HttFit) {
                        std::vector<double> dummy_result(5,-999.);
                        return dummy_result;
                    }

                    double dau1_pt, dau1_eta, dau1_phi, dau1_mass, dau2_pt, dau2_eta, dau2_phi, dau2_mass;
                    int DM1=-1, DM2=-1;
                    if (pairType == 0) {
                        dau1_pt   = muon_pt.at(dau1_index);
                        dau1_eta  = muon_eta.at(dau1_index);
                        dau1_phi  = muon_phi.at(dau1_index);
                        dau1_mass = muon_mass.at(dau1_index);
                        dau2_pt   = tau_pt.at(dau2_index);
                        dau2_eta  = tau_eta.at(dau2_index);
                        dau2_phi  = tau_phi.at(dau2_index);
                        dau2_mass = tau_mass.at(dau2_index);
                        DM2       = Tau_decayMode.at(dau2_index);
                    } else if (pairType == 1) {
                        dau1_pt   = electron_pt.at(dau1_index);
                        dau1_eta  = electron_eta.at(dau1_index);
                        dau1_phi  = electron_phi.at(dau1_index);
                        dau1_mass = electron_mass.at(dau1_index);
                        dau2_pt   = tau_pt.at(dau2_index);
                        dau2_eta  = tau_eta.at(dau2_index);
                        dau2_phi  = tau_phi.at(dau2_index);
                        dau2_mass = tau_mass.at(dau2_index);
                        DM2       = Tau_decayMode.at(dau2_index);
                    } else if (pairType == 2) {
                        dau1_pt   = tau_pt.at(dau1_index);
                        dau1_eta  = tau_eta.at(dau1_index);
                        dau1_phi  = tau_phi.at(dau1_index);
                        dau1_mass = tau_mass.at(dau1_index);
                        DM1       = Tau_decayMode.at(dau1_index);
                        dau2_pt   = tau_pt.at(dau2_index);
                        dau2_eta  = tau_eta.at(dau2_index);
                        dau2_phi  = tau_phi.at(dau2_index);
                        dau2_mass = tau_mass.at(dau2_index);
                        DM2       = Tau_decayMode.at(dau2_index);
                    } else if (pairType == 3) {
                        dau1_pt   = muon_pt.at(dau1_index);
                        dau1_eta  = muon_eta.at(dau1_index);
                        dau1_phi  = muon_phi.at(dau1_index);
                        dau1_mass = muon_mass.at(dau1_index);
                        dau2_pt   = muon_pt.at(dau2_index);
                        dau2_eta  = muon_eta.at(dau2_index);
                        dau2_phi  = muon_phi.at(dau2_index);
                        dau2_mass = muon_mass.at(dau2_index);
                    } else if (pairType == 4) {
                        dau1_pt   = electron_pt.at(dau1_index);
                        dau1_eta  = electron_eta.at(dau1_index);
                        dau1_phi  = electron_phi.at(dau1_index);
                        dau1_mass = electron_mass.at(dau1_index);
                        dau2_pt   = electron_pt.at(dau2_index);
                        dau2_eta  = electron_eta.at(dau2_index);
                        dau2_phi  = electron_phi.at(dau2_index);
                        dau2_mass = electron_mass.at(dau2_index);
                    } else if (pairType == 5) {
                        dau1_pt   = muon_pt.at(dau1_index);
                        dau1_eta  = muon_eta.at(dau1_index);
                        dau1_phi  = muon_phi.at(dau1_index);
                        dau1_mass = muon_mass.at(dau1_index);
                        dau2_pt   = electron_pt.at(dau2_index);
                        dau2_eta  = electron_eta.at(dau2_index);
                        dau2_phi  = electron_phi.at(dau2_index);
                        dau2_mass = electron_mass.at(dau2_index);
                    }

                    return httfit.FitAndGetResultWithInputs(0, pairType, DM1, DM2,
                        dau1_pt, dau1_eta, dau1_phi, dau1_mass,
                        dau2_pt, dau2_eta, dau2_phi, dau2_mass,
                        met_pt, met_phi, met_covXX, met_covXY, met_covYY);
                }
            """)

    def run(self, df):
        p = "H" if not self.isZZAnalysis else "Z"
        branches = ["%stt_svfit_pt%s"     % (p, self.systs),
                    "%stt_svfit_eta%s"    % (p, self.systs),
                    "%stt_svfit_phi%s"    % (p, self.systs),
                    "%stt_svfit_mass%s"   % (p, self.systs),
                    "%stt_svfit_timing%s" % (p, self.systs)]
        all_branches = df.GetColumnNames()
        if branches[0] in all_branches:
            return df, []

        df = df.Define("httfit_result%s" % self.systs,
            "compute_httfit({5}, pairType, dau1_index, dau2_index, "
                "Muon_pt{0}, Muon_eta, Muon_phi, Muon_mass{0}, "
                "Electron_pt{1}, Electron_eta, Electron_phi, Electron_mass{1}, "
                "Tau_pt{2}, Tau_eta, Tau_phi, Tau_mass{2}, Tau_decayMode, "
                "MET{4}_pt{3}, MET{4}_phi{3}, MET_covXX, MET_covXY, MET_covYY)".format(
                    self.muon_syst, self.electron_syst, self.tau_syst, self.met_syst,
                    self.met_smear_tag, "true" if self.bypass_HttFit else "false")).Define(
            "%stt_svfit_pt%s"     % (p, self.systs), "httfit_result%s[0]" % self.systs).Define(
            "%stt_svfit_eta%s"    % (p, self.systs), "httfit_result%s[1]" % self.systs).Define(
            "%stt_svfit_phi%s"    % (p, self.systs), "httfit_result%s[2]" % self.systs).Define(
            "%stt_svfit_mass%s"   % (p, self.systs), "httfit_result%s[3]" % self.systs).Define(
            "%stt_svfit_timing%s" % (p, self.systs), "httfit_result%s[4]" % self.systs)
        return df, branches


def HttFitRDF(*args, **kwargs):
    # Fit the H->tautau system with SVFit or FastMTT
    return lambda: HttFitRDFProducer(*args, **kwargs)
