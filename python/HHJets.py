import os
from array import array

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection, Object
from analysis_tools.utils import import_root
from Tools.Tools.jet_utils import JetPair
from Tools.Tools.utils_trigger import get_trigger_requests
from Base.Modules.baseModules import JetLepMetSyst

ROOT = import_root()


class HHJetsRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(HHJetsRDFProducer, self).__init__(self, *args, **kwargs)
        self.extEtaAcc = kwargs.pop("extEtaAcc")
        self.btag_algo = kwargs.pop("btag_algo")
        self.btag_algo_cut = kwargs.pop("btag_algo_cut", "medium")
        self.btag_algo_wps = kwargs.pop("btag_algo_wps")
        self.bypass_HHBTag = kwargs.pop("bypass_HHBTag", False)
        self.bpair_filter = kwargs.pop("bpair_filter")
        self.subjet_match = kwargs.pop("subjet_match")
        self.applyTrg = kwargs.pop("applyTrg", True)
        self.applyOfln = kwargs.pop("applyOfln", True)
        self.NanoAODv = kwargs.pop("NanoAODv")

        if not os.getenv("_HHJets"):
            os.environ["_HHJets"] = "HHJets"

            if "/libToolsTools.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gSystem.Load("libToolsTools.so")
            if os.path.expandvars("$CMT_SCRAM_ARCH") == "slc7_amd64_gcc10":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc10/"
                    "external/eigen/d812f411c3f9-cms/include/")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc10/external/"
                    "tensorflow/2.5.0/include/")
            elif os.path.expandvars("$CMT_SCRAM_ARCH") == "slc7_amd64_gcc820":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc820/"
                    "external/eigen/d812f411c3f9-bcolbf/include/eigen3")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc820/"
                    "external/tensorflow/2.1.0-bcolbf/include")

            elif os.path.expandvars("$CMT_SCRAM_ARCH") == "el9_amd64_gcc12":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/el9_amd64_gcc12/"
                    "external/eigen/3bb6a48d8c171cf20b5f8e48bfb4e424fbd4f79e-ff59ad69bf3dc401976591897b386b6a/include/eigen3")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/el9_amd64_gcc12/"
                    "external/tensorflow/2.12.0-56ca17f4dd95f093a501c2ddea2603f2/include")
            elif os.path.expandvars("$CMT_SCRAM_ARCH") == "slc7_amd64_gcc12":
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc12/"
                    "external/eigen/3bb6a48d8c171cf20b5f8e48bfb4e424fbd4f79e-ff59ad69bf3dc401976591897b386b6a/include/eigen3")
                ROOT.gROOT.ProcessLine(".include /cvmfs/cms.cern.ch/slc7_amd64_gcc12/"
                    "external/tensorflow/2.12.0-a78fc3db7a6d73e4417418af1388a3c7/include")
            else:
                raise ValueError("Architecture not considered")

            base = "{}/{}/src/Tools/Tools".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))

            ROOT.gROOT.ProcessLine(".L {}/interface/HHJetsInterface.h".format(base))

            self.year = kwargs.pop("year")
            base_hhbtag = "{}/{}/src/HHTools/HHbtag".format(
                os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
            models = [base_hhbtag + "/models/HHbtag_v3_par_%i" % i for i in range(2)]

            ROOT.gInterpreter.Declare("""
                auto HHJets = HHJetsInterface("%s", "%s", %s, %i, %s, %f, %s, %s);
            """ % (models[0], models[1], int(self.year), self.extEtaAcc,
                   "true" if self.bypass_HHBTag else "false",
                   self.btag_algo_wps[self.btag_algo_cut],
                   "true" if self.applyTrg else "false",
                   "true" if self.applyOfln else "false"))

    def run(self, df):
        df = df.Define("hlt_ditaujet_jetsRequest", "get_triggers(run, %s, %s)" %
                        (get_trigger_requests(self.year, self.NanoAODv, self.isMC, "HLT_DiTauJet", "jets")))
        
        df = df.Define("hlt_quadjet_jetsRequest", "get_triggers(run, %s, %s)" %
                        (get_trigger_requests(self.year, self.NanoAODv, self.isMC, "HLT_QuadJet", "jets")))
        
        df = df.Define("hlt_vbf_jetsRequest", "get_triggers(run, %s, %s)" %
                        (get_trigger_requests(self.year, self.NanoAODv, self.isMC, "HLT_VBF", "jets")))

        # use the old subjet matching approach
        if self.subjet_match:
            raise ValueError("The old approach matching two AK4 subjets to the "
                             "boosted AK8 jet has been decomissioned.")

        # use the new fatjet without subjet apporoach
        else:
            # use the chs jets with puID
            if self.year <= 2018:
                df = df.Define("HHJets", "HHJets.GetHHJetsWrapper(event, true, "
                    "Jet_pt{5}, Jet_eta, Jet_phi, Jet_mass{5}, "
                    "Jet_puId, Jet_jetId, Jet_btag{6}, "
                    "FatJet_pt{5}, FatJet_eta, FatJet_phi, FatJet_mass{5}, "
                    "FatJet_msoftdrop, FatJet_particleNetWithMass_HbbvsQCD, "
                    "pairType, dau1_index, dau2_index, "
                    "Muon_pt{0}, Muon_eta, Muon_phi, Muon_mass{0}, "
                    "Electron_pt{1}, Electron_eta, Electron_phi, Electron_mass{1}, "
                    "Tau_pt{2}, Tau_eta, Tau_phi, Tau_mass{2}, "
                    "MET{4}_pt{3}, MET{4}_phi{3})".format(
                        self.muon_syst, self.electron_syst, self.tau_syst, self.met_syst,
                        self.met_smear_tag, self.jet_syst, self.btag_algo))
            # use the puppi jets without puID
            else:
                df = df.Define("HHJets", "HHJets.GetHHJetsWrapper(event, false, "
                    "Jet_pt{5}, Jet_eta, Jet_phi, Jet_mass{5}, "
                    "Jet_jetId, " # dummy not used for PUPPI
                    "Jet_jetId, Jet_btag{6}, "
                    "FatJet_pt{5}, FatJet_eta, FatJet_phi, FatJet_mass{5}, "
                    "FatJet_msoftdrop, FatJet_particleNetWithMass_HbbvsQCD, "
                    "pairType, dau1_index, dau2_index, "
                    "Muon_pt{0}, Muon_eta, Muon_phi, Muon_mass{0}, "
                    "Electron_pt{1}, Electron_eta, Electron_phi, Electron_mass{1}, "
                    "Tau_pt{2}, Tau_eta, Tau_phi, Tau_mass{2}, "
                    "MET{4}_pt{3}, MET{4}_phi{3}, "
                    "isTauTauJetTrigger, isQuadJetTrigger, isVBFtrigger,"
                    "TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi, "
                    "hlt_ditaujet_jetsRequest, hlt_quadjet_jetsRequest, "
                    "hlt_vbf_jetsRequest)".format(
                        self.muon_syst, self.electron_syst, self.tau_syst, self.met_syst,
                        self.met_smear_tag, self.jet_syst, self.btag_algo))

        df = df.Define("Jet_HHbtag", "HHJets.hhbtag")
        df = df.Define("bjet1_JetIdx", "HHJets.bjet_idx1")
        df = df.Define("bjet2_JetIdx", "HHJets.bjet_idx2")

        df = df.Define("VBFjet1_JetIdx", "HHJets.vbfjet_idx1")
        df = df.Define("VBFjet2_JetIdx", "HHJets.vbfjet_idx2")

        df = df.Define("ctjet_indexes", "HHJets.ctjet_indexes")
        df = df.Define("fwjet_indexes", "HHJets.fwjet_indexes")

        df = df.Define("isBoosted", "HHJets.isBoosted")
        df = df.Define("fatjet_JetIdx", "HHJets.fatjet_idx")

        if self.bpair_filter:
            df = df.Filter("bjet1_JetIdx >= 0 || isBoosted")
        return df, ["Jet_HHbtag", "bjet1_JetIdx", "bjet2_JetIdx",
                    "VBFjet1_JetIdx", "VBFjet2_JetIdx", "ctjet_indexes",
                    "fwjet_indexes", "isBoosted", "fatjet_JetIdx"]


def HHJetsRDF(**kwargs):
    """
    Returns the HHbtag output, the indexes from the 2 bjets and 2 vbfjets (if existing),
    the indexes of the additional central and forward jets (if existing) and if the
    event has a boosted topology.

    Lepton and jet systematics (used for pt and mass variables) can be modified using the parameters
    from :ref:`BaseModules_JetLepMetSyst`.

    :param extEtaAcc: extend bjet eta cut to 2.5 in Run-3 instead of 2.4 for Run-2
    :type extEtaAcc: bool

    :param btag_algo: btag algorithm to apply
    :type btag_algo: string

    :param btag_algo_wps: working points of the btag algorithm
    :type btag_algo_wps: string

    :param btag_algo_cut: working point to be applied for the btag algorithm
    :type btag_algo_cut: string

    :param bypass_HHBTag: apply HHBTag algorithm or bypass it
    :type bypass_HHBTag: bool

    :param bpair_filter: whether to filter out output events if they don't have 2 bjet candidates
    :type bpair_filter: bool

    :param subjet_match: whether to match AK4 subjets to AK8 jet
    :type subjet_match: bool

    :param tau_syst: suffix to variable branch name
    :type tau_syst: string

    :param jet_syst: suffix to variable branch name
    :type jet_syst: string

    :param met_smear_tag: suffix to variable branch name
    :type met_smear_tag: string


    YAML sintax:

    .. code-block:: yaml

        codename:
            name: HHJetsRDF
            path: Tools.Tools.HHJets
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                extEtaAcc: True/False
                btag_algo: self.config.btag_algo
                btag_algo_wps: self.config.btag_algo_wps
                btag_algo_cut: "M"
                bypass_HHBTag: False/True
                bpair_filter: True/False
                subjet_match: False/True
                tau_syst: "corr_Medium"
                jet_syst: "corr"
                met_smear_tag: ""

    """
    return lambda: HHJetsRDFProducer(**kwargs)
