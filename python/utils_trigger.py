import re
import os
import json
import envyaml
from math import sqrt
from Tools.Tools.utils_lepton import deltaR
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection

def load_json(path):
        with open(path) as f:
            d = json.load(f)
        return d

def get_trigger_requests(year, NanoAODv, isMC, HLTcategory, objects):
    if year == 2016 or year == 2017 or year == 2018:
        path = os.path.expandvars("$CMSSW_BASE/src/Tools/Tools/data/HHtriggers_Run2.yaml")

    elif year == 2022 or year == 2023:
        path = os.path.expandvars("$CMSSW_BASE/src/Tools/Tools/data/HHtriggers_Run3.yaml")

    else:
        raise ValueError("Required year not implemented yet")
    
    triggerCfg = envyaml.EnvYAML(path)

    if isMC: DATAorMC = "MC"
    else:    DATAorMC = "DATA"

    triggerReqStr = 'trig_req{ %s, {%s}, {%s}, {%s}, {%s} }'
    triggerRRStr  = '{%d,%d}'

    triggerRequests = []
    triggerRequestsRR = []
    for HLTpath in triggerCfg["YEARS"][year][DATAorMC][HLTcategory]:
        if HLTpath == None: continue
        pathCfg = triggerCfg["HLTpaths"][HLTpath][objects]
        
        pts = [str(pt) for pt in pathCfg["pt"]]
        etas = [str(eta) for eta in pathCfg["eta"]]
        ids = [str(ID) for ID in pathCfg["id"]]

        fbits = []
        for idx, obj in enumerate(pathCfg["FilterBits"]):
            filterBits = []
            for filterBit in pathCfg["FilterBits"][obj]:
                if NanoAODv != "None":
                    filterBits.append(str(triggerCfg["FilterBits"][NanoAODv][obj[:-1]][filterBit]))
                else:
                    filterBits.append(str(triggerCfg["FilterBits"][obj[:-1]][filterBit]))

            fbits.append("{"+",".join(filterBits)+"}")

        triggerReq = triggerReqStr % (HLTpath,
                                      ",".join(pts),
                                      ",".join(etas),
                                      ",".join(ids),
                                      ",".join(fbits))

        try:
            triggerReqRR = pathCfg["runrange"]
            triggerRR = triggerRRStr % (triggerReqRR[0], triggerReqRR[1])
        except KeyError:
            triggerRR = triggerRRStr % (-1, 1E9)

        triggerRequests.append(triggerReq)
        triggerRequestsRR.append(triggerRR)

    triggerRequestsOut = "{" + ", ".join(triggerRequests) + "}"
    triggerRequestsRROut = "{" + ", ".join(triggerRequestsRR) + "}"

    return triggerRequestsRROut, triggerRequestsOut


def get_trigger_list(year, isMC, HLTcategory):
    if year == 2016 or year == 2017 or year == 2018:
        path = os.path.expandvars("$CMSSW_BASE/src/Tools/Tools/data/HHtriggers_Run2.yaml")

    elif year == 2022 or year == 2023:
        path = os.path.expandvars("$CMSSW_BASE/src/Tools/Tools/data/HHtriggers_Run3.yaml")

    else:
        raise ValueError("Required year not implemented yet")

    triggerCfg = envyaml.EnvYAML(path)
    triggerList = []
    for YEAR in triggerCfg["YEARS"]:
        for DATAorMC in ["DATA", "MC"]:
            for HLTpath in triggerCfg["YEARS"][YEAR][DATAorMC][HLTcategory]:
                triggerList.append(HLTpath)

    # remove path duplicates
    triggerList = list(dict.fromkeys(triggerList))

    return triggerList


def getSingleCrossThresholds(year):
    hltCfg  = envyaml.EnvYAML('%s/src/Tools/Tools/data/HHtriggers_Run3.yaml' % 
                                    os.environ['CMSSW_BASE'])

    SingleMuPath = "" ; isomuTrg_offMuThr = 0
    CrossMuTauPath = "" ; mutauTrg_offMuThr = 0; mutauTrg_offTauThr = 0
    SingleElePath = "" ; eleTrg_offEleThr = 0
    CrossEleTauPath = "" ; etauTrg_offEleThr = 0; etauTrg_offTauThr = 0
    DiTauPath = "" ; ditauTrg_offTauThr = 0
    DiTauJetPath = "" ; ditaujetTrg_offTauThr = 0

    # check in config what are the lower seeds
    for path in hltCfg["YEARS"][year]["DATA"]["HLT_Mu"]:
        if "IsoMu" in path:
            prvThr = 9999
            crrThr = int(path.split("IsoMu")[1].split("_")[0])
            if crrThr < prvThr:
                SingleMuPath = path
    for path in hltCfg["YEARS"][year]["DATA"]["HLT_MuTau"]:
        if "IsoMu" in path and "TauHPS" in path:
            prvThr = 9999
            crrThr = int(path.split("IsoMu")[1].split("_")[0])
            if crrThr < prvThr:
                CrossMuTauPath = path
    for path in hltCfg["YEARS"][year]["DATA"]["HLT_Ele"]:
        if "Ele" in path:
            prvThr = 9999
            crrThr = int(path.split("Ele")[1].split("_")[0])
            if crrThr < prvThr:
                SingleElePath = path
    for path in hltCfg["YEARS"][year]["DATA"]["HLT_EleTau"]:
        if "Ele" in path and "TauHPS" in path:
            prvThr = 9999
            crrThr = int(path.split("Ele")[1].split("_")[0])
            if crrThr < prvThr:
                CrossEleTauPath = path
    for path in hltCfg["YEARS"][year]["DATA"]["HLT_DiTau"]:
        if "DoubleMediumDeepTau" in path and not "PFJet" in path:
            prvThr = 9999
            crrThr = int(path.split("TauHPS")[1].split("_")[0])
            if crrThr < prvThr:
                DiTauPath = path
    for path in hltCfg["YEARS"][year]["DATA"]["HLT_DiTauJet"]:
        if "DoubleMediumDeepTau" in path and "PFJet" in path:
            prvThr = 9999
            crrThr = int(path.split("TauHPS")[1].split("_")[0])
            if crrThr < prvThr:
                DiTauJetPath = path

    if SingleMuPath    != "": isomuTrg_offMuThr     = hltCfg["HLTpaths"][SingleMuPath]["leptons"]["pt"][0]
    if CrossMuTauPath  != "":
        mutauTrg_offMuThr = hltCfg["HLTpaths"][CrossMuTauPath]["leptons"]["pt"][0]
        mutauTrg_offTauThr = hltCfg["HLTpaths"][CrossMuTauPath]["leptons"]["pt"][1]
    if SingleElePath   != "": eleTrg_offEleThr      = hltCfg["HLTpaths"][SingleElePath]["leptons"]["pt"][0]
    if CrossEleTauPath != "":
        etauTrg_offEleThr = hltCfg["HLTpaths"][CrossEleTauPath]["leptons"]["pt"][0]
        etauTrg_offTauThr = hltCfg["HLTpaths"][CrossEleTauPath]["leptons"]["pt"][1]
    if DiTauPath       != "": ditauTrg_offTauThr    = hltCfg["HLTpaths"][DiTauPath]["leptons"]["pt"][0]
    if DiTauJetPath    != "": ditaujetTrg_offTauThr = hltCfg["HLTpaths"][DiTauJetPath]["leptons"]["pt"][0]

    return (isomuTrg_offMuThr, mutauTrg_offMuThr, mutauTrg_offTauThr, eleTrg_offEleThr,
            etauTrg_offEleThr, etauTrg_offTauThr, ditauTrg_offTauThr, ditaujetTrg_offTauThr)