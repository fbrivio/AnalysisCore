#include "Tools/Tools/interface/HHLeptonInterface.h"

// Constructors

HHLeptonInterface::HHLeptonInterface (
    int vvvl_vsjet, int vl_vse, int vvl_vse, int t_vsmu, int vl_vsmu,
    bool applyTrg, bool applyOfln, bool applyOflnIso) {
  vvvl_vsjet_ = vvvl_vsjet;
  vl_vse_ = vl_vse;
  vvl_vse_ = vvl_vse;
  t_vsmu_ = t_vsmu;
  vl_vsmu_ = vl_vsmu;

  applyTrg_ = applyTrg;
  applyOfln_ = applyOfln;
  applyOflnIso_ = applyOflnIso;
};

// Destructor
HHLeptonInterface::~HHLeptonInterface() {}

// Select tautau pair and assess channel:
//  if   (tMu   == 1 && lMu  == 0  &&  tEle == 0 && lEle == 0)                              -->  0 MuTau
//  elif (tMu   == 0 && lMu  == 0  &&  tEle == 1 && lEle == 0)                              -->  1 ETau
//  elif (tMu   == 0 && lMu  == 0  &&  tEle == 0 && lEle == 0)                              -->  2 TauTau
//  elif ((tMu  >  1 || lMu  >  1) || (tMu  == 1 && lMu  == 1)) &&  tEle == 0 && lEle == 0) -->  3 MuMu
//  elif ((tEle >  1 || lEle >  1) || (tEle == 1 && lEle == 1)) &&  tMu  == 0 && lMu  == 0) -->  4 EE
//  elif ((tMu  == 1 || lMu  == 1) && (tEle == 1 || lEle == 1))                             -->  5 EMu
//  else                                                                                    --> -1
lepton_output HHLeptonInterface::get_dau_indexes(
    fRVec Muon_pt, fRVec Muon_eta, fRVec Muon_phi, fRVec Muon_mass,
    fRVec Muon_pfRelIso04_all, fRVec Muon_dxy, fRVec Muon_dz,
    bRVec Muon_mediumId, bRVec Muon_tightId, iRVec Muon_charge,
    fRVec Electron_pt, fRVec Electron_eta, fRVec Electron_phi, fRVec Electron_mass,
    bRVec Electron_mvaIso_WP80, bRVec Electron_mvaNoIso_WP90,
    bRVec Electron_mvaIso_WP90, fRVec Electron_pfRelIso03_all,
    fRVec Electron_dxy, fRVec Electron_dz, iRVec Electron_charge, fRVec Electron_mvaIso,
    fRVec Tau_pt, fRVec Tau_eta, fRVec Tau_phi, fRVec Tau_mass,
    iRVec Tau_tauIdVSmu, iRVec Tau_tauIdVSe,
    iRVec Tau_tauIdVSjet, fRVec Tau_rawTauIdVSjet, fRVec Tau_rawIso,
    fRVec Tau_dz, iRVec Tau_decayMode, iRVec Tau_charge,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi,
    std::vector<trig_req> hlt_mu_triggers, std::vector<trig_req> hlt_ele_triggers,
    std::vector<trig_req> hlt_mutau_triggers, std::vector<trig_req> hlt_eletau_triggers,
    std::vector<trig_req> hlt_ditau_triggers, std::vector<trig_req> hlt_ditaujet_triggers,
    std::vector<trig_req> hlt_quadjet_triggers, std::vector<trig_req> hlt_vbf_triggers)
{
    // Declare output
    lepton_output evt_out({
        -1, -1, -1, -1, -1, -1, -1, -1,
        -1., -1., -1., -1., -1, -1, -1, -1,
        -1., -1., -1., -1., -1, -1, -1, -1
    });

    // Select good muons with tight (MuTau events) and loose selections (MuMu events)
    std::vector<int> tightmuons;
    std::vector<int> loosemuons;
    for (size_t imuon = 0; imuon < Muon_pt.size(); imuon ++)
    {
        if (applyOflnIso_ && (fabs(Muon_dxy[imuon]) >= 0.045 || fabs(Muon_dz[imuon]) >= 0.2 || !Muon_tightId[imuon]))
            continue;

        if (applyOflnIso_)
        {
            if (fabs(Muon_eta[imuon]) <= 2.4 && Muon_pfRelIso04_all[imuon] < 0.15 && Muon_pt[imuon] >= 20)
                tightmuons.push_back(imuon);

            else if (fabs(Muon_eta[imuon]) <= 2.4 && Muon_pfRelIso04_all[imuon] < 0.30 && Muon_pt[imuon] >= 10)
                loosemuons.push_back(imuon);
        }
        else
        {
            if (Muon_pt[imuon] >= 20)
                tightmuons.push_back(imuon);
            else if (Muon_pt[imuon] >= 10)
                loosemuons.push_back(imuon);
        }
    }

    // Select good electrons with tight (ETau events) and loose selections (EE events)
    std::vector<int> tightelectrons;
    std::vector<int> looseelectrons;
    for (size_t iele = 0; iele < Electron_pt.size(); iele ++)
    {
        if (applyOflnIso_ && (fabs(Electron_dxy[iele]) >= 0.045 || fabs(Electron_dz[iele]) >= 0.2 || !Electron_mvaIso_WP80[iele]))
            continue;

        if (applyOflnIso_)
        {
            if (fabs(Electron_eta[iele]) <= 2.5 && Electron_pfRelIso03_all[iele] < 0.1 && Electron_pt[iele] >= 24)
                tightelectrons.push_back(iele);

            else if (fabs(Electron_eta[iele]) <= 2.5 && Electron_pfRelIso03_all[iele] < 0.3 && Electron_pt[iele] >= 10)
                looseelectrons.push_back(iele);
        }
        else
        {
            if (Electron_pt[iele] >= 24)
                tightelectrons.push_back(iele);
            else if (Electron_pt[iele] >= 10)
                looseelectrons.push_back(iele);
        }
    }

    // 0 - Start MuTau channel
    if (tightmuons.size() == 1 && loosemuons.size() == 0 && tightelectrons.size() == 0 && looseelectrons.size() == 0)
    {
        // Select good taus with loose pT cut (20 GeV)
        std::vector<int> goodtaus;
        for (size_t itau = 0; itau < Tau_pt.size(); itau ++)
        {
            if (applyOflnIso_)
            {
                if (Tau_tauIdVSmu[itau] < t_vsmu_ || Tau_tauIdVSe[itau] < vl_vse_ || Tau_tauIdVSjet[itau] < vvvl_vsjet_)
                    continue;
                if (fabs(Tau_dz[itau]) > 0.2)
                    continue;
                if (Tau_decayMode[itau] != 0 && Tau_decayMode[itau] != 1 &&
                    Tau_decayMode[itau] != 10 && Tau_decayMode[itau] != 11)
                    continue;
                if (fabs(Tau_eta[itau]) >= 2.3)
                    continue;
                if (Tau_pt[itau] <= 20.)
                    continue;
            }
            goodtaus.push_back(itau);
        }

        // MuTau pairs
        if (goodtaus.size() >= 1)
        {
            std::vector<tau_pair> mutau_pairs;
            for (auto & imuon: tightmuons)
            {
                auto muon_tlv = TLorentzVector();
                muon_tlv.SetPtEtaPhiM(Muon_pt[imuon], Muon_eta[imuon], Muon_phi[imuon], Muon_mass[imuon]);

                for (auto & itau: goodtaus)
                {
                    auto tau_tlv = TLorentzVector();
                    tau_tlv.SetPtEtaPhiM(Tau_pt[itau], Tau_eta[itau], Tau_phi[itau], Tau_mass[itau]);

                    if (tau_tlv.DeltaR(muon_tlv) < 0.5)
                        continue;

                    if (applyTrg_
                        // check SingleMu triggers
                        && !pass_trigger(applyOfln_, {muon_tlv},
                                         hlt_mu_triggers, TrigObj_id, TrigObj_filterBits,
                                         TrigObj_eta, TrigObj_phi)
                        // check CrossMuTau triggers
                        && !pass_trigger(applyOfln_, {muon_tlv,  tau_tlv},
                                         hlt_mutau_triggers, TrigObj_id, TrigObj_filterBits,
                                         TrigObj_eta, TrigObj_phi)
                        )
                        continue;

                    mutau_pairs.push_back(tau_pair(
                        {imuon, Muon_pfRelIso04_all[imuon], Muon_pt[imuon],
                         itau, Tau_rawTauIdVSjet[itau], Tau_pt[itau], 0, 0, 0}
                    ));
                }
            }

            // Check if any MuTau pair is good enough
            if (mutau_pairs.size() > 0)
            {
                std::stable_sort(mutau_pairs.begin(), mutau_pairs.end(), pairSortHybrid);
                int nleps = lepton_veto(mutau_pairs[0].index1, -1, -1, -1,
                    Muon_pt, Muon_eta, Muon_dz, Muon_dxy,
                    Muon_pfRelIso04_all, Muon_mediumId, Muon_tightId,
                    Electron_pt, Electron_eta, Electron_dz, Electron_dxy,
                    Electron_mvaNoIso_WP90, Electron_mvaIso_WP90,
                    Electron_pfRelIso03_all);

                int ind1 = mutau_pairs[0].index1;
                int ind2 = mutau_pairs[0].index2;
                int isOS = (int) (Muon_charge[ind1] != Tau_charge[ind2]);

                evt_out.update(0, ind1, ind2, 0, 0, 0, isOS, nleps,
                    Muon_eta[ind1], Muon_phi[ind1], Muon_pfRelIso04_all[ind1], -1., -1, -1, -1, -1,
                    Tau_eta[ind2], Tau_phi[ind2], -1., -1., Tau_decayMode[ind2],
                    Tau_tauIdVSe[ind2], Tau_tauIdVSmu[ind2], Tau_tauIdVSjet[ind2]);
            }
        }
    } // end MuTau channel

    // 1 - Start ETau channel
    else if (tightmuons.size() == 0 && loosemuons.size() == 0 && tightelectrons.size() == 1 && looseelectrons.size() == 0)
    {
        // Select good taus with loose pT cut (20 GeV)
        std::vector<int> goodtaus;
        for (size_t itau = 0; itau < Tau_pt.size(); itau ++)
        {
            if (applyOflnIso_)
            {
                if (Tau_tauIdVSmu[itau] < t_vsmu_ || Tau_tauIdVSe[itau] < vl_vse_ || Tau_tauIdVSjet[itau] < vvvl_vsjet_)
                    continue;
                if (fabs(Tau_dz[itau]) > 0.2)
                    continue;
                if (Tau_decayMode[itau] != 0 && Tau_decayMode[itau] != 1 &&
                    Tau_decayMode[itau] != 10 && Tau_decayMode[itau] != 11)
                    continue;
                if (fabs(Tau_eta[itau]) >= 2.3)
                    continue;
                if (Tau_pt[itau] <= 20.)
                    continue;
            }
            goodtaus.push_back(itau);
        }

        // ETau pairs
        if (goodtaus.size() >= 1)
        {
            std::vector<tau_pair> etau_pairs;
            for (auto & iele: tightelectrons)
            {
                auto electron_tlv = TLorentzVector();
                electron_tlv.SetPtEtaPhiM(Electron_pt[iele], Electron_eta[iele],
                                          Electron_phi[iele], Electron_mass[iele]);

                for (auto & itau: goodtaus)
                {
                    auto tau_tlv = TLorentzVector();
                    tau_tlv.SetPtEtaPhiM(Tau_pt[itau], Tau_eta[itau], Tau_phi[itau], Tau_mass[itau]);

                    if (tau_tlv.DeltaR(electron_tlv) < 0.5)
                        continue;

                    if (applyTrg_
                        // check SingleEle triggers
                        && !pass_trigger(applyOfln_, {electron_tlv},
                                         hlt_ele_triggers, TrigObj_id, TrigObj_filterBits,
                                         TrigObj_eta, TrigObj_phi)
                        // check CrossEleTau triggers
                        && !pass_trigger(applyOfln_, {electron_tlv,  tau_tlv},
                                         hlt_eletau_triggers, TrigObj_id, TrigObj_filterBits,
                                         TrigObj_eta, TrigObj_phi)
                        )
                        continue;

                    etau_pairs.push_back(tau_pair(
                        {iele, Electron_pfRelIso03_all[iele], Electron_pt[iele],
                         itau, Tau_rawTauIdVSjet[itau], Tau_pt[itau], 0, 0, 0}
                    ));
                }
            }

            // Check if any ETau pair is good enough
            if (etau_pairs.size() > 0)
            {
                std::stable_sort(etau_pairs.begin(), etau_pairs.end(), pairSortHybrid);
                int nleps = lepton_veto(-1, -1, etau_pairs[0].index1, -1,
                    Muon_pt, Muon_eta, Muon_dz, Muon_dxy,
                    Muon_pfRelIso04_all, Muon_mediumId, Muon_tightId,
                    Electron_pt, Electron_eta, Electron_dz, Electron_dxy,
                    Electron_mvaNoIso_WP90, Electron_mvaIso_WP90,
                    Electron_pfRelIso03_all);

                int ind1 = etau_pairs[0].index1;
                int ind2 = etau_pairs[0].index2;
                int isOS = (int) (Electron_charge[ind1] != Tau_charge[ind2]);

                evt_out.update(1, ind1, ind2, 0, 0, 0, isOS, nleps,
                    Electron_eta[ind1], Electron_phi[ind1], Electron_pfRelIso03_all[ind1], Electron_mvaIso[ind1], -1, -1, -1, -1,
                    Tau_eta[ind2], Tau_phi[ind2], -1., -1., Tau_decayMode[ind2],
                    Tau_tauIdVSe[ind2], Tau_tauIdVSmu[ind2], Tau_tauIdVSjet[ind2]);
            }
        }
    } // end ETau channel

    // 2 - Start TauTau channel
    else if (tightmuons.size() == 0 && loosemuons.size() == 0 && tightelectrons.size() == 0 && looseelectrons.size() == 0)
    {
        // Select good taus
        std::vector<int> goodtaus;
        for (size_t itau = 0; itau < Tau_pt.size(); itau ++)
        {
            if (applyOflnIso_)
            {
                if (Tau_tauIdVSmu[itau] < vl_vsmu_ || Tau_tauIdVSe[itau] < vvl_vse_ || Tau_tauIdVSjet[itau] < vvvl_vsjet_)
                    continue;
                if (fabs(Tau_dz[itau]) > 0.2)
                    continue;
                if (Tau_decayMode[itau] != 0 && Tau_decayMode[itau] != 1 &&
                    Tau_decayMode[itau] != 10 && Tau_decayMode[itau] != 11)
                    continue;
                if (fabs(Tau_eta[itau]) >= 2.3)
                    continue;
                if (Tau_pt[itau] <= 20.)
                    continue;
            }
            goodtaus.push_back(itau);
        }

        // TauTau pairs
        std::vector<tau_pair> tautau_pairs;
        for (auto & itau1 : goodtaus)
        {
            auto tau1_tlv = TLorentzVector();
            tau1_tlv.SetPtEtaPhiM(Tau_pt[itau1], Tau_eta[itau1], Tau_phi[itau1], Tau_mass[itau1]);

            for (auto & itau2 : goodtaus)
            {
                if (itau1 == itau2)
                    continue;

                auto tau2_tlv = TLorentzVector();
                tau2_tlv.SetPtEtaPhiM(Tau_pt[itau2], Tau_eta[itau2], Tau_phi[itau2], Tau_mass[itau2]);

                int pass_tautaujet = 0;
                int pass_quadjet = 0;
                int pass_vbf = 0;

                if (applyTrg_
                    // check DiTau triggers
                    && !pass_trigger(applyOfln_, {tau1_tlv,  tau2_tlv},
                                     hlt_ditau_triggers, TrigObj_id, TrigObj_filterBits,
                                     TrigObj_eta, TrigObj_phi))
                {
                    // check DiTau+Jet triggers
                    pass_tautaujet = pass_trigger(applyOfln_, {tau1_tlv,  tau2_tlv},
                                        hlt_ditaujet_triggers, TrigObj_id, TrigObj_filterBits,
                                        TrigObj_eta, TrigObj_phi);

                    // FIXME check QuadJet triggers
                    // pass_quadjet = pass_trigger(applyOfln_, {tau1_tlv, tau2_tlv},
                    //                     hlt_quadjet_triggers, TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi);

                    pass_vbf = pass_trigger(applyOfln_, {tau1_tlv,  tau2_tlv},
                                    hlt_vbf_triggers, TrigObj_id, TrigObj_filterBits,
                                    TrigObj_eta, TrigObj_phi);

                    // FIXME for quadjet
                    if (!pass_tautaujet && !pass_vbf) /* && !pass_quadjet */
                        continue;
                }

                tautau_pairs.push_back(tau_pair(
                    {itau1, Tau_rawTauIdVSjet[itau1], Tau_pt[itau1],
                     itau2, Tau_rawTauIdVSjet[itau2], Tau_pt[itau2],
                     pass_tautaujet, pass_quadjet, pass_vbf}
                ));
            }
        }

        // Check if any TauTau pair is good enough
        if (tautau_pairs.size() > 0)
        {
            std::stable_sort(tautau_pairs.begin(), tautau_pairs.end(), pairSortMVA);
            int nleps = lepton_veto(-1, -1, -1, -1,
                Muon_pt, Muon_eta, Muon_dz, Muon_dxy,
                Muon_pfRelIso04_all, Muon_mediumId, Muon_tightId,
                Electron_pt, Electron_eta, Electron_dz, Electron_dxy,
                Electron_mvaNoIso_WP90, Electron_mvaIso_WP90,
                Electron_pfRelIso03_all);

            int ind1 = tautau_pairs[0].index1;
            int ind2 = tautau_pairs[0].index2;
            int isOS = (int) (Tau_charge[ind1] != Tau_charge[ind2]);

            evt_out.update(2, ind1, ind2,
                tautau_pairs[0].isTauTauJetTrigger, tautau_pairs[0].isQuadJetTrigger, tautau_pairs[0].isVBFtrigger,
                isOS, nleps, Tau_eta[ind1], Tau_phi[ind1], -1., -1.,
                Tau_decayMode[ind1], Tau_tauIdVSe[ind1], Tau_tauIdVSmu[ind1], Tau_tauIdVSjet[ind1],
                Tau_eta[ind2], Tau_phi[ind2], -1., -1.,
                Tau_decayMode[ind2], Tau_tauIdVSe[ind2], Tau_tauIdVSmu[ind2], Tau_tauIdVSjet[ind2]);
        }
    } // end TauTau channel

    // 3 - Start MuMu channel
    else if ( (tightmuons.size() > 1 || loosemuons.size() > 1 || (tightmuons.size() == 1 && loosemuons.size() == 1))
              && tightelectrons.size() == 0 && looseelectrons.size() == 0)
    {
        // Merge tight and loose muons
        std::vector<int> allmuons = tightmuons;
        allmuons.insert(allmuons.end(), loosemuons.begin(), loosemuons.end());

        // MuMu pairs
        std::vector<tau_pair> mumu_pairs;
        for (auto & i : allmuons)
        {
            // Now tighten iso selection
            if (applyOflnIso_ && Muon_pfRelIso04_all[i] >= 0.15)
                continue;

            for (auto & j : allmuons)
            {
                // Same selections on second muon
                if (applyOflnIso_ && Muon_pfRelIso04_all[j] >= 0.15)
                    continue;

                auto mu1_tlv = TLorentzVector();
                mu1_tlv.SetPtEtaPhiM(Muon_pt[i], Muon_eta[i], Muon_phi[i], Muon_mass[i]);
                auto mu2_tlv = TLorentzVector();
                mu2_tlv.SetPtEtaPhiM(Muon_pt[j], Muon_eta[j], Muon_phi[j], Muon_mass[j]);

                if (mu1_tlv.DeltaR(mu2_tlv) < 0.5)
                    continue;

                if (applyTrg_
                    // check SingleMu triggers on first muon
                    && !pass_trigger(applyOfln_, {mu1_tlv},
                                     hlt_mu_triggers, TrigObj_id, TrigObj_filterBits,
                                     TrigObj_eta, TrigObj_phi)
                    // check SingleMu triggers on second muon
                    && !pass_trigger(applyOfln_, {mu2_tlv},
                                     hlt_mu_triggers, TrigObj_id, TrigObj_filterBits,
                                     TrigObj_eta, TrigObj_phi)
                    )
                    continue;

                mumu_pairs.push_back(tau_pair(
                    {int(i), Muon_pfRelIso04_all[i], Muon_pt[i],
                     int(j), Muon_pfRelIso04_all[j], Muon_pt[j], 0, 0, 0}
                ));
            }
        }

        // Check if any MuMu pair is good enough
        if (mumu_pairs.size() > 0)
        {
            std::stable_sort(mumu_pairs.begin(), mumu_pairs.end(), pairSortRawIso);
            int nleps = lepton_veto(mumu_pairs[0].index1, mumu_pairs[0].index2, -1, -1,
                Muon_pt, Muon_eta, Muon_dz, Muon_dxy,
                Muon_pfRelIso04_all, Muon_mediumId, Muon_tightId,
                Electron_pt, Electron_eta, Electron_dz, Electron_dxy,
                Electron_mvaNoIso_WP90, Electron_mvaIso_WP90,
                Electron_pfRelIso03_all);

            int ind1 = mumu_pairs[0].index1;
            int ind2 = mumu_pairs[0].index2;
            int isOS = (int) (Muon_charge[ind1] != Muon_charge[ind2]);

            evt_out.update(3, ind1, ind2, 0, 0, 0, isOS, nleps,
                Muon_eta[ind1], Muon_phi[ind1], Muon_pfRelIso04_all[ind1], -1., -1, -1, -1, -1,
                Muon_eta[ind2], Muon_phi[ind2], Muon_pfRelIso04_all[ind2], -1., -1, -1, -1, -1);
        }
    } // end MuMu channel

    // 4 - Start EE channel
    else if ( (tightelectrons.size() > 1 || looseelectrons.size() > 1 || (tightelectrons.size() == 1 && looseelectrons.size() == 1))
              && tightmuons.size() == 0 && loosemuons.size() == 0)
    {
        // Merge tight and loose electrons
        std::vector<int> allelectrons = tightelectrons;
        allelectrons.insert(allelectrons.end(), looseelectrons.begin(), looseelectrons.end());

        // EE pairs
        std::vector<tau_pair> ee_pairs;
        for (auto & i : allelectrons)
        {
            // Now tighten iso selection, and relax MVA iso
            if (applyOflnIso_ && (Electron_pfRelIso03_all[i] >= 0.15 || !Electron_mvaIso_WP90[i]))
                continue;

            for (auto & j : allelectrons)
            {
                // Same selections on second electron
                if (applyOflnIso_ && (Electron_pfRelIso03_all[j] >= 0.15 || !Electron_mvaIso_WP90[j]))
                    continue;

                auto ele1_tlv = TLorentzVector();
                ele1_tlv.SetPtEtaPhiM(Electron_pt[i], Electron_eta[i], Electron_phi[i], Electron_mass[i]);
                auto ele2_tlv = TLorentzVector();
                ele2_tlv.SetPtEtaPhiM(Electron_pt[j], Electron_eta[j], Electron_phi[j], Electron_mass[j]);

                if (ele1_tlv.DeltaR(ele2_tlv) < 0.5)
                    continue;

                if (applyTrg_
                    // check SingleEle triggers on first electron
                    && !pass_trigger(applyOfln_, {ele1_tlv},
                                     hlt_ele_triggers, TrigObj_id, TrigObj_filterBits,
                                     TrigObj_eta, TrigObj_phi)
                    // check SingleEle triggers on second electron
                    && !pass_trigger(applyOfln_, {ele2_tlv},
                                     hlt_ele_triggers, TrigObj_id, TrigObj_filterBits,
                                     TrigObj_eta, TrigObj_phi)
                    )
                    continue;

                ee_pairs.push_back(tau_pair(
                    {int(i), Electron_pfRelIso03_all[i], Electron_pt[i],
                     int(j), Electron_pfRelIso03_all[j], Electron_pt[j], 0, 0, 0}
                ));
            }
        }

        // Check if any EE pair is good enough
        if (ee_pairs.size() > 0)
        {
            std::stable_sort(ee_pairs.begin(), ee_pairs.end(), pairSortRawIso);
            int nleps = lepton_veto(-1, -1, ee_pairs[0].index1, ee_pairs[0].index2,
                Muon_pt, Muon_eta, Muon_dz, Muon_dxy,
                Muon_pfRelIso04_all, Muon_mediumId, Muon_tightId,
                Electron_pt, Electron_eta, Electron_dz, Electron_dxy,
                Electron_mvaNoIso_WP90, Electron_mvaIso_WP90,
                Electron_pfRelIso03_all);

            int ind1 = ee_pairs[0].index1;
            int ind2 = ee_pairs[0].index2;
            int isOS = (int) (Electron_charge[ind1] != Electron_charge[ind2]);

            evt_out.update(4, ind1, ind2, 0, 0, 0, isOS, nleps,
                Electron_eta[ind1], Electron_phi[ind1], Electron_pfRelIso03_all[ind1], Electron_mvaIso[ind1], -1, -1, -1, -1,
                Electron_eta[ind2], Electron_phi[ind2], Electron_pfRelIso03_all[ind2], Electron_mvaIso[ind2], -1, -1, -1, -1);
        }
    } // end EE channel

    // 5 - Start EMu channel
    else if ( (tightmuons.size() == 1 || loosemuons.size() == 1) && (tightelectrons.size() == 1 || looseelectrons.size() == 1) )
    {
        // Merge tight and loose muons
        std::vector<int> allmuons = tightmuons;
        allmuons.insert(allmuons.end(), loosemuons.begin(), loosemuons.end());

        // Merge tight and loose electrons
        std::vector<int> allelectrons = tightelectrons;
        allelectrons.insert(allelectrons.end(), looseelectrons.begin(), looseelectrons.end());

        // EMu pairs
        std::vector<tau_pair> emu_pairs;
        for (auto & i : allmuons)
        {
            // Now tighten muon iso selection
            if (applyOflnIso_ && Muon_pfRelIso04_all[i] >= 0.15)
                continue;

            for (auto & j : allelectrons)
            {
                // Now tighten iso selection, and relax MVA iso
                if (applyOflnIso_ && (Electron_pfRelIso03_all[j] >= 0.15 || !Electron_mvaIso_WP90[j]))
                    continue;

                auto mu_tlv = TLorentzVector();
                mu_tlv.SetPtEtaPhiM(Muon_pt[i], Muon_eta[i], Muon_phi[i], Muon_mass[i]);
                auto ele_tlv = TLorentzVector();
                ele_tlv.SetPtEtaPhiM(Electron_pt[j], Electron_eta[j], Electron_phi[j], Electron_mass[j]);

                if (mu_tlv.DeltaR(ele_tlv) < 0.5)
                    continue;

                if (applyTrg_
                    // check SingleMu triggers on muon
                    && !pass_trigger(applyOfln_, {mu_tlv},
                                     hlt_mu_triggers, TrigObj_id, TrigObj_filterBits,
                                     TrigObj_eta, TrigObj_phi)
                    // check SingleEle triggers on electron
                    && !pass_trigger(applyOfln_, {ele_tlv},
                                     hlt_ele_triggers, TrigObj_id, TrigObj_filterBits,
                                     TrigObj_eta, TrigObj_phi)
                    )
                    continue;

                emu_pairs.push_back(tau_pair(
                    {int(i), Muon_pfRelIso04_all[i], Muon_pt[i],
                     int(j), Electron_pfRelIso03_all[j], Electron_pt[j], 0, 0, 0}
                ));
            }
        }

        // Check if any EMu pair is good enough
        if (emu_pairs.size() > 0)
        {
            std::stable_sort(emu_pairs.begin(), emu_pairs.end(), pairSortRawIso);
            int nleps = lepton_veto(emu_pairs[0].index1, -1, emu_pairs[0].index2, -1,
                Muon_pt, Muon_eta, Muon_dz, Muon_dxy,
                Muon_pfRelIso04_all, Muon_mediumId, Muon_tightId,
                Electron_pt, Electron_eta, Electron_dz, Electron_dxy,
                Electron_mvaNoIso_WP90, Electron_mvaIso_WP90,
                Electron_pfRelIso03_all);

            int ind1 = emu_pairs[0].index1;
            int ind2 = emu_pairs[0].index2;
            int isOS = (int) (Muon_charge[ind1] != Electron_charge[ind2]);

            evt_out.update(5, ind1, ind2, 0, 0, 0, isOS, nleps,
                Muon_eta[ind1], Muon_phi[ind1], Muon_pfRelIso04_all[ind1], -1., -1, -1, -1, -1,
                Electron_eta[ind2], Electron_phi[ind2], Electron_pfRelIso03_all[ind2], Electron_mvaIso[ind2], -1, -1, -1, -1);
        }
    } // end EMu channel

    // Final return
    return evt_out;
}

int HHLeptonInterface::lepton_veto(int muon1_index, int muon2_index,
  int electron1_index, int electron2_index,
  fRVec Muon_pt, fRVec Muon_eta, fRVec Muon_dz, fRVec Muon_dxy,
  fRVec Muon_pfRelIso04_all, bRVec Muon_mediumId, bRVec Muon_tightId,
  fRVec Electron_pt, fRVec Electron_eta, fRVec Electron_dz, fRVec Electron_dxy,
  bRVec Electron_mvaNoIso_WP90, bRVec Electron_mvaIso_WP90,
  fRVec Electron_pfRelIso03_all)
{
    int nleps = 0;

    // Check extra muons
    for (size_t imuon = 0; imuon < Muon_pt.size(); imuon++)
    {
        // Exclude muon(s) already selected in the pair
        if ((int) imuon == muon1_index || (int) imuon == muon2_index)
            continue;
        // Exclude muons that don't pass loose selections
        if (fabs(Muon_eta[imuon]) > 2.4 || Muon_pt[imuon] < 10 || fabs(Muon_dz[imuon]) > 0.2
            || fabs(Muon_dxy[imuon]) > 0.045 || Muon_pfRelIso04_all[imuon] > 0.3)
            continue;
        // Tight does not imply Medium so we check both
        if (!Muon_mediumId[imuon] and !Muon_tightId[imuon])
            continue;
        // If any muon passes the above selections: add it to extra leptons
        nleps++;
    }

    // Check extra electrons
    for (size_t iele = 0; iele < Electron_pt.size(); iele++)
    {
        // Exclude electron(s) already selected in the pair
        if ((int) iele == electron1_index || (int) iele == electron2_index )
            continue;
        // Exclude electrons that don't pass loose selections
        if (fabs(Electron_eta[iele]) > 2.5 || Electron_pt[iele] < 10 || fabs(Electron_dz[iele]) > 0.2
            || fabs(Electron_dxy[iele]) > 0.045)
            continue;
        // We check (MVAMedium OR (noIso-MVAMedium AND relIso < 0.3))
        if (!(Electron_pfRelIso03_all[iele] < 0.3 && Electron_mvaNoIso_WP90[iele])
            && !Electron_mvaIso_WP90[iele])
            continue;
        // If any electron passes the above selections: add it to extra leptons
        nleps++;
    }
    return nleps;
}
