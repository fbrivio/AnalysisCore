#include "Tools/Tools/interface/HHGenInterface.h"
#include <iostream>

HHGenInterface::HHGenInterface() {}

HHGenInterface::~HHGenInterface() {}

gen_reco_output HHGenInterface::initFromGenInfo(gen_output genInput)
{
    gen_reco_output genrecoOut;

    genrecoOut.PairType = genInput.PairType;
    genrecoOut.H1_idx   = genInput.H1_idx;
    genrecoOut.H1_pt    = genInput.H1_pt;
    genrecoOut.H1_eta   = genInput.H1_eta;
    genrecoOut.H1_phi   = genInput.H1_phi;
    genrecoOut.H1_mass  = genInput.H1_mass;
    genrecoOut.H2_idx   = genInput.H2_idx;
    genrecoOut.H2_pt    = genInput.H2_pt;
    genrecoOut.H2_eta   = genInput.H2_eta;
    genrecoOut.H2_phi   = genInput.H2_phi;
    genrecoOut.H2_mass  = genInput.H2_mass;
    genrecoOut.L1_idx   = genInput.L1_idx;
    genrecoOut.L1_pt    = genInput.L1_pt;
    genrecoOut.L1_eta   = genInput.L1_eta;
    genrecoOut.L1_phi   = genInput.L1_phi;
    genrecoOut.L1_mass  = genInput.L1_mass;
    genrecoOut.L1_pdgid = genInput.L1_pdgid;
    genrecoOut.L2_idx   = genInput.L2_idx;
    genrecoOut.L2_pt    = genInput.L2_pt;
    genrecoOut.L2_eta   = genInput.L2_eta;
    genrecoOut.L2_phi   = genInput.L2_phi;
    genrecoOut.L2_mass  = genInput.L2_mass;
    genrecoOut.L2_pdgid = genInput.L2_pdgid;
    genrecoOut.B1_idx   = genInput.B1_idx;
    genrecoOut.B1_pt    = genInput.B1_pt;
    genrecoOut.B1_eta   = genInput.B1_eta;
    genrecoOut.B1_phi   = genInput.B1_phi;
    genrecoOut.B1_mass  = genInput.B1_mass;
    genrecoOut.B1_pdgid = genInput.B1_pdgid;
    genrecoOut.B2_idx   = genInput.B2_idx;
    genrecoOut.B2_pt    = genInput.B2_pt;
    genrecoOut.B2_eta   = genInput.B2_eta;
    genrecoOut.B2_phi   = genInput.B2_phi;
    genrecoOut.B2_mass  = genInput.B2_mass;
    genrecoOut.B2_pdgid = genInput.B2_pdgid;
    genrecoOut.HH_pt    = genInput.HH_pt;
    genrecoOut.HH_eta   = genInput.HH_eta;
    genrecoOut.HH_phi   = genInput.HH_phi;
    genrecoOut.HH_mass  = genInput.HH_mass;
    genrecoOut.HH_cts   = genInput.HH_cts;
    genrecoOut.LL_pt    = genInput.LL_pt;
    genrecoOut.LL_eta   = genInput.LL_eta;
    genrecoOut.LL_phi   = genInput.LL_phi;
    genrecoOut.LL_mass  = genInput.LL_mass;
    genrecoOut.BB_pt    = genInput.BB_pt;
    genrecoOut.BB_eta   = genInput.BB_eta;
    genrecoOut.BB_phi   = genInput.BB_phi;
    genrecoOut.BB_mass  = genInput.BB_mass;

    return genrecoOut;
}

reco_objects HHGenInterface::lep_gen2reco(
        iRVec GenPart_pdgId, iRVec GenPart_status, iRVec GenPart_statusFlags, iRVec GenPart_genPartIdxMother,
        gen_output genInput, iRVec GenVisTau_genPartIdxMother,
        fRVec GenVisTau_pt, fRVec GenVisTau_eta, fRVec GenVisTau_phi, fRVec GenVisTau_mass,
        iRVec l1genPartIdx, fRVec l1pt, fRVec l1eta, fRVec l1phi, fRVec l1mass,
        iRVec l2genPartIdx, fRVec l2pt, fRVec l2eta, fRVec l2phi, fRVec l2mass)
{
    reco_objects recoOut;

    TLorentzVector genlep1;
    TLorentzVector genlep2;
    genlep1.SetPtEtaPhiM(genInput.L1_pt, genInput.L1_eta, genInput.L1_phi, genInput.L1_mass);
    genlep2.SetPtEtaPhiM(genInput.L2_pt, genInput.L2_eta, genInput.L2_phi, genInput.L2_mass);

    for (size_t idx = 0; idx < GenVisTau_genPartIdxMother.size(); idx++) {
        int visIdx = GenVisTau_genPartIdxMother[idx];
        if (visIdx == genInput.L2_idx) {
            genlep2.SetPtEtaPhiM(GenVisTau_pt[idx], GenVisTau_eta[idx], 
                                 GenVisTau_phi[idx], GenVisTau_mass[idx]);
        }

        if (genInput.PairType == 2) {
            if (int(visIdx) == genInput.L1_idx) {
                genlep1.SetPtEtaPhiM(GenVisTau_pt[idx], GenVisTau_eta[idx], 
                                     GenVisTau_phi[idx], GenVisTau_mass[idx]);
            }
        }
    }

    // First lepton gen matching
    for (size_t idx = 0; idx < l1pt.size(); idx++) {
        TLorentzVector lep1;
        lep1.SetPtEtaPhiM(l1pt[idx], l1eta[idx], l1phi[idx], l1mass[idx]);

        if (genInput.PairType != 2) {
            int genIdx = l1genPartIdx[idx];
            if (genIdx >= 0) {
                int pdg = GenPart_pdgId[genIdx];
                bool isLast = (GenPart_statusFlags[genIdx] & (1<<13)) ? true : false;
                bool isDirectPromptTauDecayProduct = (GenPart_statusFlags[genIdx] & (1<<5)) ? true : false;
                int mthIdx = GenPart_genPartIdxMother[genIdx];

                bool mthIsFromHardScatt = false;
                if (mthIdx > -1) {
                    bool mthIsLast = (GenPart_statusFlags[mthIdx] & (1<<13)) ? true : false;
                    bool isFromHardProcess = (GenPart_statusFlags[mthIdx] & (1<<8)) ? true : false;
                    mthIsFromHardScatt = (mthIsLast && isFromHardProcess);
                }

                if ( ( (abs(pdg) == 13 && genInput.PairType == 0) || (abs(pdg) == 13 && genInput.PairType == 3) ||
                       (abs(pdg) == 13 && genInput.PairType == 5) || (abs(pdg) == 11 && genInput.PairType == 1) ||
                       (abs(pdg) == 11 && genInput.PairType == 4) || (abs(pdg) == 11 && genInput.PairType == 5))
                     && isLast && isDirectPromptTauDecayProduct && mthIsFromHardScatt
                     && genlep1.DeltaR(lep1) < 0.5) {
                    recoOut.C1_idx  = idx;
                    recoOut.C1_pt   = lep1.Pt();
                    recoOut.C1_eta  = lep1.Eta();
                    recoOut.C1_phi  = lep1.Phi();
                    recoOut.C1_mass = lep1.M();

                    // found perfect gen match break loop
                    break;
                }
            }
        }
        else {
            int genVisIdx = l1genPartIdx[idx];
            if (genVisIdx < 0 or genVisIdx >= int(GenVisTau_genPartIdxMother.size())) { continue; }

            int genIdx = GenVisTau_genPartIdxMother[genVisIdx];
            if (genIdx < 0 or genIdx >= int(GenPart_pdgId.size())) { continue; }

            int pdg = GenPart_pdgId[genIdx];
            int sts = GenPart_status[genIdx];

            if (abs(pdg) == 15 && sts == 2 && genlep1.DeltaR(lep1) < 0.5) {
                recoOut.C1_idx  = idx;
                recoOut.C1_pt   = lep1.Pt();
                recoOut.C1_eta  = lep1.Eta();
                recoOut.C1_phi  = lep1.Phi();
                recoOut.C1_mass = lep1.M();

                // found perfect gen match break loop
                break;
            }
        }

        // did not find perfect gen match, look for reco in cone
        if (genlep1.DeltaR(lep1) < 0.5 && lep1.Pt() > recoOut.C1_pt) {
                recoOut.C1_idx  = idx;
                recoOut.C1_pt   = lep1.Pt();
                recoOut.C1_eta  = lep1.Eta();
                recoOut.C1_phi  = lep1.Phi();
                recoOut.C1_mass = lep1.M();
        }
    }

    // Second lepton gen matching
    for (size_t idx = 0; idx < l2pt.size(); idx++) {
        TLorentzVector lep2;
        lep2.SetPtEtaPhiM(l2pt[idx], l2eta[idx], l2phi[idx], l2mass[idx]);

        if (genInput.PairType != 2) {
            int genIdx = l2genPartIdx[idx];
            if (genIdx >= 0) {
                int pdg = GenPart_pdgId[genIdx];
                bool isLast = (GenPart_statusFlags[genIdx] & (1<<13)) ? true : false;
                bool isDirectPromptTauDecayProduct = (GenPart_statusFlags[genIdx] & (1<<5)) ? true : false;
                int mthIdx = GenPart_genPartIdxMother[genIdx];

                bool mthIsFromHardScatt = false;
                if (mthIdx > -1) {
                    bool mthIsLast = (GenPart_statusFlags[mthIdx] & (1<<13)) ? true : false;
                    bool isFromHardProcess = (GenPart_statusFlags[mthIdx] & (1<<8)) ? true : false;
                    mthIsFromHardScatt = (mthIsLast && isFromHardProcess);
                }

                if ( ( (abs(pdg) == 13 && genInput.PairType == 0) || (abs(pdg) == 13 && genInput.PairType == 3) ||
                       (abs(pdg) == 13 && genInput.PairType == 5) || (abs(pdg) == 11 && genInput.PairType == 1) ||
                       (abs(pdg) == 11 && genInput.PairType == 4) || (abs(pdg) == 11 && genInput.PairType == 5))
                     && isLast && isDirectPromptTauDecayProduct && mthIsFromHardScatt
                     && genlep2.DeltaR(lep2) < 0.5) {
                    recoOut.C2_idx  = idx;
                    recoOut.C2_pt   = lep2.Pt();
                    recoOut.C2_eta  = lep2.Eta();
                    recoOut.C2_phi  = lep2.Phi();
                    recoOut.C2_mass = lep2.M();

                    // found perfect gen match break loop
                    break;
                }
            }
        }
        else {
            int genVisIdx = l2genPartIdx[idx];
            if (genVisIdx < 0 or genVisIdx >= int(GenVisTau_genPartIdxMother.size())) { continue; }

            int genIdx = GenVisTau_genPartIdxMother[genVisIdx];
            if (genIdx < 0 or genIdx >= int(GenPart_pdgId.size())) { continue; }

            int pdg = GenPart_pdgId[genIdx];
            int sts = GenPart_status[genIdx];

            if (abs(pdg) == 15 && sts == 2 && genlep2.DeltaR(lep2) < 0.5) {
                recoOut.C2_idx  = idx;
                recoOut.C2_pt   = lep2.Pt();
                recoOut.C2_eta  = lep2.Eta();
                recoOut.C2_phi  = lep2.Phi();
                recoOut.C2_mass = lep2.M();

                // found perfect gen match break loop
                break;
            }
        }

        // did not find perfect gen match, look for reco in cone
        if (genlep2.DeltaR(lep2) < 0.5 && lep2.Pt() > recoOut.C2_pt) {
                recoOut.C2_idx  = idx;
                recoOut.C2_pt   = lep2.Pt();
                recoOut.C2_eta  = lep2.Eta();
                recoOut.C2_phi  = lep2.Phi();
                recoOut.C2_mass = lep2.M();
        }
    }

    return recoOut;
}

gen_output HHGenInterface::get_gen_info(
    iRVec GenPart_statusFlags, iRVec GenPart_pdgId, 
    iRVec GenPart_status, iRVec GenPart_genPartIdxMother,
    fRVec GenPart_pt, fRVec GenPart_eta, fRVec GenPart_phi, fRVec GenPart_mass)
{
    gen_output genOut;

    int H1_idx = -99;
    int H2_idx = -99;

    bool Tau1DecaysToLepton = false;
    bool Tau2DecaysToLepton = false;

    for (size_t idx = 0; idx < GenPart_pdgId.size(); idx++) {
        bool isLast = (GenPart_statusFlags[idx] & (1<<13)) ? true : false;
        bool isDirectPromptTauDecayProduct = (GenPart_statusFlags[idx] & (1<<5)) ? true : false;
        bool isHardProcess = (GenPart_statusFlags[idx] & (1<<7)) ? true : false;
        bool isFromHardProcess = (GenPart_statusFlags[idx] & (1<<8)) ? true : false;
        int pdg = GenPart_pdgId[idx];
        int sts = GenPart_status[idx];
        int mthIdx = GenPart_genPartIdxMother[idx];

        bool mthIsFromHardProcess = false;
        bool mthIsStatus2tau = false;
        int mthPdg = 0;
        if (mthIdx > -1) {
            mthIsFromHardProcess = (GenPart_statusFlags[mthIdx] & (1<<8)) ? true : false;
            bool mthIsStatus2 = (GenPart_status[mthIdx] == 2) ? true : false;
            bool mthIsTau = (abs(GenPart_pdgId[mthIdx]) == 15) ? true : false;
            mthIsStatus2tau = (mthIsStatus2 && mthIsTau);
            mthPdg = GenPart_pdgId[mthIdx];
        }

        if (abs(pdg) == 25 && sts == 22) {
            if (H1_idx != -99 && H2_idx != -99) {
                std::cout << "** ERROR: more than 2 H identified" << std::endl;
                continue;
            }

            (H1_idx == -99) ? (H1_idx = idx) : (H2_idx = idx);
        }

        if ((abs(pdg) == 11 || abs(pdg) == 13) && isLast && 
                isDirectPromptTauDecayProduct && mthIsFromHardProcess && mthIsStatus2tau) {
            if (pdg * genOut.L1_pdgid > 0 && !Tau1DecaysToLepton) {
                genOut.L1_idx   = idx;
                genOut.L1_pt    = GenPart_pt[idx];
                genOut.L1_eta   = GenPart_eta[idx];
                genOut.L1_phi   = GenPart_phi[idx];
                genOut.L1_mass  = GenPart_mass[idx];
                genOut.L1_pdgid = GenPart_pdgId[idx];
                Tau1DecaysToLepton = true;
            }
            else if (pdg * genOut.L2_pdgid > 0 && !Tau2DecaysToLepton) {
                genOut.L2_idx   = idx;
                genOut.L2_pt    = GenPart_pt[idx];
                genOut.L2_eta   = GenPart_eta[idx];
                genOut.L2_phi   = GenPart_phi[idx];
                genOut.L2_mass  = GenPart_mass[idx];
                genOut.L2_pdgid = GenPart_pdgId[idx];
                Tau2DecaysToLepton = true;
            }
            else {
                std::cout << "** ERROR: more than 2 gen tau decay products identified" << std::endl;
                continue;
            }
        }

        if (abs(pdg) == 15 && sts == 2 && isFromHardProcess) {
            if (genOut.L1_idx != -99 && genOut.L2_idx != -99) {
                std::cout << "** ERROR: more than 2 gen tau identified" << std::endl;
                continue;
            }

            if (genOut.L1_idx == -99) {
                genOut.L1_idx    = idx;
                genOut.L1_pt     = GenPart_pt[idx];
                genOut.L1_eta    = GenPart_eta[idx];
                genOut.L1_phi    = GenPart_phi[idx];
                genOut.L1_mass   = GenPart_mass[idx];
                genOut.L1_pdgid  = GenPart_pdgId[idx];
            }
            else {
                genOut.L2_idx   = idx;
                genOut.L2_pt    = GenPart_pt[idx];
                genOut.L2_eta   = GenPart_eta[idx];
                genOut.L2_phi   = GenPart_phi[idx];
                genOut.L2_mass  = GenPart_mass[idx];
                genOut.L2_pdgid = GenPart_pdgId[idx];
            }
        }

        if (abs(pdg) == 5 && isHardProcess && abs(mthPdg) == 25) {
            if (genOut.B1_idx != -99 && genOut.B2_idx != -99) {
                std::cout << "** ERROR: more than 2 gen b identified" << std::endl;
                continue;
            }

            if (genOut.B1_idx == -99) {
                genOut.B1_idx   = idx;
                genOut.B1_pt    = GenPart_pt[idx];
                genOut.B1_eta   = GenPart_eta[idx];
                genOut.B1_phi   = GenPart_phi[idx];
                genOut.B1_mass  = GenPart_mass[idx];
                genOut.B1_pdgid = GenPart_pdgId[idx];
            }
            else {
                genOut.B2_idx   = idx;
                genOut.B2_pt    = GenPart_pt[idx];
                genOut.B2_eta   = GenPart_eta[idx];
                genOut.B2_phi   = GenPart_phi[idx];
                genOut.B2_mass  = GenPart_mass[idx];
                genOut.B2_pdgid = GenPart_pdgId[idx];
            }
        }

        if (genOut.L1_pdgid != -99 && genOut.L2_pdgid != -99 
                && genOut.L1_pdgid * genOut.L2_pdgid > 0) {
            std::cout << "** ERROR: gen decay products have the same charge" << std::endl;
            continue;
        }
    }

    auto H1 = TLorentzVector();
    auto H2 = TLorentzVector();
    auto HH = TLorentzVector();
    H1.SetPtEtaPhiM(GenPart_pt[H1_idx], GenPart_eta[H1_idx],
                    GenPart_phi[H1_idx], GenPart_mass[H1_idx]);
    H2.SetPtEtaPhiM(GenPart_pt[H2_idx], GenPart_eta[H2_idx],
                    GenPart_phi[H2_idx], GenPart_mass[H2_idx]);
    HH = H1 + H2;

    genOut.HH_pt   = HH.Pt();
    genOut.HH_eta  = HH.Eta();
    genOut.HH_phi  = HH.Phi();
    genOut.HH_mass = HH.M();

    // cos(theta*)
    H1.Boost(-HH.BoostVector());
    genOut.HH_cts = H1.CosTheta();

    // order L1 and L2 to have light leptons always in L1
    if (abs(genOut.L1_pdgid) == 15) {
        int tmp1_idx    = genOut.L1_idx;
        float tmp1_pt   = genOut.L1_pt;
        float tmp1_eta  = genOut.L1_eta;
        float tmp1_phi  = genOut.L1_phi;
        float tmp1_mass = genOut.L1_mass;
        int tmp1_pdgid  = genOut.L1_pdgid;

        genOut.L1_idx   = genOut.L2_idx;
        genOut.L1_pt    = genOut.L2_pt;
        genOut.L1_eta   = genOut.L2_eta;
        genOut.L1_phi   = genOut.L2_phi;
        genOut.L1_mass  = genOut.L2_mass;
        genOut.L1_pdgid = genOut.L2_pdgid;

        genOut.L2_idx   = tmp1_idx;
        genOut.L2_pt    = tmp1_pt;
        genOut.L2_eta   = tmp1_eta;
        genOut.L2_phi   = tmp1_phi;
        genOut.L2_mass  = tmp1_mass;
        genOut.L2_pdgid = tmp1_pdgid;
    }

    if (abs(GenPart_pdgId[genOut.L1_idx]) == 13) {
        if      (abs(GenPart_pdgId[genOut.L2_idx]) == 11) { genOut.PairType = 5; }
        else if (abs(GenPart_pdgId[genOut.L2_idx]) == 13) { genOut.PairType = 3; }
        else                                              { genOut.PairType = 0; }
    }
    else if (abs(GenPart_pdgId[genOut.L1_idx]) == 11) {
        if      (abs(GenPart_pdgId[genOut.L2_idx]) == 11) { genOut.PairType = 4; }
        else if (abs(GenPart_pdgId[genOut.L2_idx]) == 13) { genOut.PairType = 5; }
        else                                              { genOut.PairType = 1; }
    }
    else                                                  { genOut.PairType = 2; }

    auto L1 = TLorentzVector();
    auto L2 = TLorentzVector();
    auto LL = TLorentzVector();
    L1.SetPtEtaPhiM(genOut.L1_pt, genOut.L1_eta, genOut.L1_phi, genOut.L1_mass);
    L2.SetPtEtaPhiM(genOut.L2_pt, genOut.L2_eta, genOut.L2_phi, genOut.L2_mass);
    LL = L1 + L2;

    genOut.LL_pt   = LL.Pt();
    genOut.LL_eta  = LL.Eta();
    genOut.LL_phi  = LL.Phi();
    genOut.LL_mass = LL.M();

    auto B1 = TLorentzVector();
    auto B2 = TLorentzVector();
    auto BB = TLorentzVector();
    B1.SetPtEtaPhiM(genOut.B1_pt, genOut.B1_eta, genOut.B1_phi, genOut.B1_mass);
    B2.SetPtEtaPhiM(genOut.B2_pt, genOut.B2_eta, genOut.B2_phi, genOut.B2_mass);
    BB = B1 + B2;
    genOut.BB_pt   = BB.Pt();
    genOut.BB_eta  = BB.Eta();
    genOut.BB_phi  = BB.Phi();
    genOut.BB_mass = BB.M();

    if (H1.DeltaR(BB) > H2.DeltaR(BB)) {
        genOut.H1_idx  = H1_idx;
        genOut.H1_pt   = GenPart_pt[H1_idx];
        genOut.H1_eta  = GenPart_eta[H1_idx];
        genOut.H1_phi  = GenPart_phi[H1_idx];
        genOut.H1_mass = GenPart_mass[H1_idx];

        genOut.H2_idx  = H2_idx;
        genOut.H2_pt   = GenPart_pt[H2_idx];
        genOut.H2_eta  = GenPart_eta[H2_idx];
        genOut.H2_phi  = GenPart_phi[H2_idx];
        genOut.H2_mass = GenPart_mass[H2_idx];
    }
    else {
        genOut.H1_idx  = H2_idx;
        genOut.H1_pt   = GenPart_pt[H2_idx];
        genOut.H1_eta  = GenPart_eta[H2_idx];
        genOut.H1_phi  = GenPart_phi[H2_idx];
        genOut.H1_mass = GenPart_mass[H2_idx];

        genOut.H2_idx  = H1_idx;
        genOut.H2_pt   = GenPart_pt[H1_idx];
        genOut.H2_eta  = GenPart_eta[H1_idx];
        genOut.H2_phi  = GenPart_phi[H1_idx];
        genOut.H2_mass = GenPart_mass[H1_idx];
    }

    return genOut;
}

gen_reco_output HHGenInterface::get_gen_reco(
    iRVec GenPart_statusFlags, iRVec GenPart_pdgId, 
    iRVec GenPart_status, iRVec GenPart_genPartIdxMother,
    fRVec GenPart_pt, fRVec GenPart_eta, fRVec GenPart_phi, fRVec GenPart_mass,
    iRVec GenVisTau_genPartIdxMother, iRVec GenVisTau_status,
    fRVec GenVisTau_pt, fRVec GenVisTau_eta, fRVec GenVisTau_phi, fRVec GenVisTau_mass,
    iRVec Muon_genPartIdx, fRVec Muon_pt, fRVec Muon_eta, fRVec Muon_phi, fRVec Muon_mass,
    iRVec Electron_genPartIdx, fRVec Electron_pt, fRVec Electron_eta, fRVec Electron_phi, fRVec Electron_mass,
    iRVec Tau_genPartIdx, fRVec Tau_pt, fRVec Tau_eta, fRVec Tau_phi, fRVec Tau_mass,
    iRVec GenJet_partonFlavour, fRVec GenJet_pt, fRVec GenJet_eta, fRVec GenJet_phi, fRVec GenJet_mass)
{

    gen_output genOut = get_gen_info(GenPart_statusFlags, GenPart_pdgId, 
                                     GenPart_status, GenPart_genPartIdxMother,
                                     GenPart_pt, GenPart_eta, GenPart_phi, GenPart_mass);

    gen_reco_output genrecoOut = initFromGenInfo(genOut);

    reco_objects recoOut;
    if (genOut.PairType == 0) {
        recoOut = lep_gen2reco(GenPart_pdgId, GenPart_statusFlags, GenPart_status, GenPart_genPartIdxMother,
                               genOut, GenVisTau_genPartIdxMother,
                               GenVisTau_pt, GenVisTau_eta, GenVisTau_phi, GenVisTau_mass,
                               Muon_genPartIdx, Muon_pt, Muon_eta, Muon_phi, Muon_mass,
                               Tau_genPartIdx, Tau_pt, Tau_eta, Tau_phi, Tau_mass);
    }
    else if (genOut.PairType == 1) {
        recoOut = lep_gen2reco(GenPart_pdgId, GenPart_statusFlags, GenPart_status, GenPart_genPartIdxMother,
                               genOut, GenVisTau_genPartIdxMother,
                               GenVisTau_pt, GenVisTau_eta, GenVisTau_phi, GenVisTau_mass,
                               Electron_genPartIdx, Electron_pt, Electron_eta, Electron_phi, Electron_mass,
                               Tau_genPartIdx, Tau_pt, Tau_eta, Tau_phi, Tau_mass);
    }
    else if (genOut.PairType == 2) {
        recoOut = lep_gen2reco(GenPart_pdgId, GenPart_statusFlags, GenPart_status, GenPart_genPartIdxMother,
                               genOut, GenVisTau_genPartIdxMother,
                               GenVisTau_pt, GenVisTau_eta, GenVisTau_phi, GenVisTau_mass,
                               Tau_genPartIdx, Tau_pt, Tau_eta, Tau_phi, Tau_mass,
                               Tau_genPartIdx, Tau_pt, Tau_eta, Tau_phi, Tau_mass);
    }
    else if (genOut.PairType == 3) {
        recoOut = lep_gen2reco(GenPart_pdgId, GenPart_statusFlags, GenPart_status, GenPart_genPartIdxMother,
                               genOut, GenVisTau_genPartIdxMother,
                               GenVisTau_pt, GenVisTau_eta, GenVisTau_phi, GenVisTau_mass,
                               Muon_genPartIdx, Muon_pt, Muon_eta, Muon_phi, Muon_mass,
                               Muon_genPartIdx, Muon_pt, Muon_eta, Muon_phi, Muon_mass);
    }
    else if (genOut.PairType == 4) {
        recoOut = lep_gen2reco(GenPart_pdgId, GenPart_statusFlags, GenPart_status, GenPart_genPartIdxMother,
                               genOut, GenVisTau_genPartIdxMother,
                               GenVisTau_pt, GenVisTau_eta, GenVisTau_phi, GenVisTau_mass,
                               Electron_genPartIdx, Electron_pt, Electron_eta, Electron_phi, Electron_mass,
                               Electron_genPartIdx, Electron_pt, Electron_eta, Electron_phi, Electron_mass);
    }
    else if (genOut.PairType == 5) {
        recoOut = lep_gen2reco(GenPart_pdgId, GenPart_statusFlags, GenPart_status, GenPart_genPartIdxMother,
                               genOut, GenVisTau_genPartIdxMother,
                               GenVisTau_pt, GenVisTau_eta, GenVisTau_phi, GenVisTau_mass,
                               Muon_genPartIdx, Muon_pt, Muon_eta, Muon_phi, Muon_mass,
                               Electron_genPartIdx, Electron_pt, Electron_eta, Electron_phi, Electron_mass);
    }

    genrecoOut.T1_idx  = recoOut.C1_idx;
    genrecoOut.T1_pt   = recoOut.C1_pt;
    genrecoOut.T1_eta  = recoOut.C1_eta;
    genrecoOut.T1_phi  = recoOut.C1_phi;
    genrecoOut.T1_mass = recoOut.C1_mass;

    genrecoOut.T2_idx  = recoOut.C2_idx;
    genrecoOut.T2_pt   = recoOut.C2_pt;
    genrecoOut.T2_eta  = recoOut.C2_eta;
    genrecoOut.T2_phi  = recoOut.C2_phi;
    genrecoOut.T2_mass = recoOut.C2_mass;

    auto T1 = TLorentzVector();
    auto T2 = TLorentzVector();
    auto TT = TLorentzVector();
    T1.SetPtEtaPhiM(genrecoOut.T1_pt, genrecoOut.T1_eta, genrecoOut.T1_phi, genrecoOut.T1_mass);
    T2.SetPtEtaPhiM(genrecoOut.T2_pt, genrecoOut.T2_eta, genrecoOut.T2_phi, genrecoOut.T2_mass);
    TT = T1 + T2;

    genrecoOut.TT_pt   = TT.Pt();
    genrecoOut.TT_eta  = TT.Eta();
    genrecoOut.TT_phi  = TT.Phi();
    genrecoOut.TT_mass = TT.M();

    auto bquark1 = TLorentzVector();
    auto bquark2 = TLorentzVector();
    bquark1.SetPtEtaPhiM(genOut.B1_pt, genOut.B1_eta, genOut.B1_phi, genOut.B1_mass);
    bquark2.SetPtEtaPhiM(genOut.B2_pt, genOut.B2_eta, genOut.B2_phi, genOut.B2_mass);

    for (size_t idx = 0; idx < GenJet_pt.size(); ++idx) {
        if (abs(GenJet_partonFlavour[idx]) != 5) { continue; }

        auto jet = TLorentzVector();
        jet.SetPtEtaPhiM(GenJet_pt[idx], GenJet_eta[idx], GenJet_phi[idx], GenJet_mass[idx]);

        if (bquark1.DeltaR(jet) < 0.5 && genrecoOut.J1_pt) {
            genrecoOut.J1_idx  = idx;
            genrecoOut.J1_pt   = jet.Pt();
            genrecoOut.J1_eta  = jet.Eta();
            genrecoOut.J1_phi  = jet.Phi();
            genrecoOut.J1_mass = jet.M();
        }
        else if (bquark2.DeltaR(jet) < 0.5 && genrecoOut.J2_pt) {
            genrecoOut.J2_idx  = idx;
            genrecoOut.J2_pt   = jet.Pt();
            genrecoOut.J2_eta  = jet.Eta();
            genrecoOut.J2_phi  = jet.Phi();
            genrecoOut.J2_mass = jet.M();
        }
    }

    auto J1 = TLorentzVector();
    auto J2 = TLorentzVector();
    auto JJ = TLorentzVector();
    J1.SetPtEtaPhiM(genrecoOut.J1_pt, genrecoOut.J1_eta, genrecoOut.J1_phi, genrecoOut.J1_mass);
    J2.SetPtEtaPhiM(genrecoOut.J2_pt, genrecoOut.J2_eta, genrecoOut.J2_phi, genrecoOut.J2_mass);
    JJ = J1 + J2;

    genrecoOut.JJ_pt   = JJ.Pt();
    genrecoOut.JJ_eta  = JJ.Eta();
    genrecoOut.JJ_phi  = JJ.Phi();
    genrecoOut.JJ_mass = JJ.M();
    
    return genrecoOut;
}
