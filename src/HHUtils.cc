#include "Tools/Tools/interface/HHutils.h"

//------------------------------------------------------------------------
// stuctures and functions needed for trigger checking and matching
bool match_trigger_object(float off_eta, float off_phi, int obj_id,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi,
    std::vector<int> bits)
{
  for (size_t iobj = 0; iobj < TrigObj_id.size(); iobj++) {
    if (TrigObj_id[iobj] != obj_id) continue;
    auto const dPhi(std::abs(reco::deltaPhi(off_phi, TrigObj_phi[iobj])));
    auto const dEta(std::abs(off_eta - TrigObj_eta[iobj]));
    auto const delR2(dPhi * dPhi + dEta * dEta);
    if (delR2 > 0.5 * 0.5)
      continue;
    bool matched_bits = true;
    for (auto & bit : bits) {
      if ((TrigObj_filterBits[iobj] & (1<<bit)) == 0) {
        matched_bits = false;
        break;
      }
    }
    if (!matched_bits)
      continue;
    return true;
  }
  return false;
}

bool pass_trigger(
    bool applyOfln, std::vector<TLorentzVector> off_objs, std::vector<trig_req> triggers,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi)
{
    for (auto &trigger: triggers) {
        if (!trigger.pass)
            continue;

        int passObjs = 0;
        int totObjs = int( trigger.id.size() );

        for (int idx = 0; idx < totObjs; ++idx) {
            if (applyOfln &&
                 (off_objs.at(idx).Pt() < trigger.pt.at(idx) || 
                  abs(off_objs.at(idx).Eta()) > trigger.eta.at(idx)))
                continue;

            if (!match_trigger_object(off_objs.at(idx).Eta(), off_objs.at(idx).Phi(), trigger.id.at(idx),
                TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi, trigger.bits.at(idx)))
                continue;

            passObjs += 1;
        }

        if (passObjs == totObjs)
            return true;
    }
    return false;
}

//------------------------------------------------------------------------
// stuctures and functions needed mainly for HHLeptonInterface

// pairSorting for MVA isolations: higher score --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortMVA (const tau_pair& pA, const tau_pair& pB)
{
  float isoA = pA.iso1;
  float isoB = pB.iso1;
  if (isoA > isoB) return true;
  else if (isoA < isoB) return false;

  float ptA = pA.pt1;
  float ptB = pB.pt1;
  if (ptA > ptB) return true;
  else if (ptA < ptB) return false;

  isoA = pA.iso2;
  isoB = pB.iso2;
  if (isoA > isoB) return true;
  else if (isoA < isoB) return false;

  ptA = pA.pt2;
  ptB = pB.pt2;
  if (ptA > ptB) return true;
  else if (ptA < ptB) return false;

  // should be never here..
  return false;
}

// pairSorting for Raw isolations: lower iso value --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortRawIso (const tau_pair& pA, const tau_pair& pB)
{
  float isoA = pA.iso1;
  float isoB = pB.iso1;
  if (isoA < isoB) return true;
  else if (isoA > isoB) return false;

  float ptA = pA.pt1;
  float ptB = pB.pt1;
  if (ptA > ptB) return true;
  else if (ptA < ptB) return false;

  isoA = pA.iso2;
  isoB = pB.iso2;
  if (isoA < isoB) return true;
  else if (isoA > isoB) return false;

  ptA = pA.pt2;
  ptB = pB.pt2;
  if (ptA > ptB) return true;
  else if (ptA < ptB) return false;

  // should be never here..
  return false;
}

// pairSorting for Lep+Tauh:
//  - leg1 (lepton): lower iso value --> more isolated
//  - leg2 (tauh)  : higher score    --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortHybrid (const tau_pair& pA, const tau_pair& pB)
{
  float isoA = pA.iso1;
  float isoB = pB.iso1;
  if (isoA < isoB) return true;
  else if (isoA > isoB) return false;

  float ptA = pA.pt1;
  float ptB = pB.pt1;
  if (ptA > ptB) return true;
  else if (ptA < ptB) return false;

  isoA = pA.iso2;
  isoB = pB.iso2;
  if (isoA > isoB) return true;
  else if (isoA < isoB) return false;

  ptA = pA.pt2;
  ptB = pB.pt2;
  if (ptA > ptB) return true;
  else if (ptA < ptB) return false;

  // should be never here..
  return false;
}

//------------------------------------------------------------------------
// functions needed mainly for HHJetsInterface
bool jetSort (const jet_idx_btag& jA, const jet_idx_btag& jB)
{
  return (jA.btag > jB.btag);
}

bool jetPairSort (const jet_pair_mass& jA, const jet_pair_mass& jB)
{
  return (jA.inv_mass > jB.inv_mass);
}
