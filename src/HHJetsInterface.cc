#include "Tools/Tools/interface/HHJetsInterface.h"

// Constructor
HHJetsInterface::HHJetsInterface (std::string model_0, std::string model_1, int year, bool extEtaAcc,
                                  bool bypass_HHBTag, float btag_algo_cut,
                                  bool applyTrg, bool applyOfln):
  HHbtagger_(std::array<std::string, 2> { {model_0, model_1} })
{
  year_ = year;
  if (extEtaAcc) max_bjet_eta = 2.5;
  bypass_HHBTag_ = bypass_HHBTag;
  btag_algo_cut_ = btag_algo_cut;

  applyTrg_ = applyTrg;
  applyOfln_ = applyOfln;
}


// Destructor
HHJetsInterface::~HHJetsInterface() {}

leps HHJetsInterface::GetLeps(
  fRVec muonpt, fRVec muoneta, fRVec muonphi, fRVec muonmass,
  fRVec electronpt, fRVec electroneta, fRVec electronphi, fRVec electronmass,
  fRVec taupt, fRVec taueta, fRVec tauphi, fRVec taumass,
  int dau1_index, int dau2_index, int pairType)
{
  leps leptons;
  if (pairType == 0) { // muTau
      leptons.dau1_pt   = muonpt.at(dau1_index);
      leptons.dau1_eta  = muoneta.at(dau1_index);
      leptons.dau1_phi  = muonphi.at(dau1_index);
      leptons.dau1_mass = muonmass.at(dau1_index);
      leptons.dau2_pt   = taupt.at(dau2_index);
      leptons.dau2_eta  = taueta.at(dau2_index);
      leptons.dau2_phi  = tauphi.at(dau2_index);
      leptons.dau2_mass = taumass.at(dau2_index);
  } else if (pairType == 1) { //eTau
      leptons.dau1_pt   = electronpt.at(dau1_index);
      leptons.dau1_eta  = electroneta.at(dau1_index);
      leptons.dau1_phi  = electronphi.at(dau1_index);
      leptons.dau1_mass = electronmass.at(dau1_index);
      leptons.dau2_pt   = taupt.at(dau2_index);
      leptons.dau2_eta  = taueta.at(dau2_index);
      leptons.dau2_phi  = tauphi.at(dau2_index);
      leptons.dau2_mass = taumass.at(dau2_index);
  } else if (pairType == 2) { // tauTau
      leptons.dau1_pt   = taupt.at(dau1_index);
      leptons.dau1_eta  = taueta.at(dau1_index);
      leptons.dau1_phi  = tauphi.at(dau1_index);
      leptons.dau1_mass = taumass.at(dau1_index);
      leptons.dau2_pt   = taupt.at(dau2_index);
      leptons.dau2_eta  = taueta.at(dau2_index);
      leptons.dau2_phi  = tauphi.at(dau2_index);
      leptons.dau2_mass = taumass.at(dau2_index);
  } else if (pairType == 3) { // muMu
      leptons.dau1_pt   = muonpt.at(dau1_index);
      leptons.dau1_eta  = muoneta.at(dau1_index);
      leptons.dau1_phi  = muonphi.at(dau1_index);
      leptons.dau1_mass = muonmass.at(dau1_index);
      leptons.dau2_pt   = muonpt.at(dau2_index);
      leptons.dau2_eta  = muoneta.at(dau2_index);
      leptons.dau2_phi  = muonphi.at(dau2_index);
      leptons.dau2_mass = muonmass.at(dau2_index);
  } else if (pairType == 4) { // EE
      leptons.dau1_pt   = electronpt.at(dau1_index);
      leptons.dau1_eta  = electroneta.at(dau1_index);
      leptons.dau1_phi  = electronphi.at(dau1_index);
      leptons.dau1_mass = electronmass.at(dau1_index);
      leptons.dau2_pt   = electronpt.at(dau2_index);
      leptons.dau2_eta  = electroneta.at(dau2_index);
      leptons.dau2_phi  = electronphi.at(dau2_index);
      leptons.dau2_mass = electronmass.at(dau2_index);
  } else if (pairType == 5) { // EMu
      leptons.dau1_pt   = muonpt.at(dau1_index);
      leptons.dau1_eta  = muoneta.at(dau1_index);
      leptons.dau1_phi  = muonphi.at(dau1_index);
      leptons.dau1_mass = muonmass.at(dau1_index);
      leptons.dau2_pt   = electronpt.at(dau2_index);
      leptons.dau2_eta  = electroneta.at(dau2_index);
      leptons.dau2_phi  = electronphi.at(dau2_index);
      leptons.dau2_mass = electronmass.at(dau2_index);
  } else {
      leptons.dau1_pt = -999.;
      leptons.dau1_eta = -999.;
      leptons.dau1_phi = -999.;
      leptons.dau1_mass = -999.;
      leptons.dau2_pt = -999.;
      leptons.dau2_eta = -999.;
      leptons.dau2_phi = -999.;
      leptons.dau2_mass = -999.;
  }  return leptons;
}

// definition compatible with Run-2 analyses using CHS jets and need Jet_puId
// this definition uses the new fatjet without subjet apporoach
output HHJetsInterface::GetHHJets(
    unsigned long long int event, int pairType,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
    iRVec Jet_puId, fRVec Jet_jetId, fRVec Jet_btag,
    fRVec FatJet_pt, fRVec FatJet_eta, fRVec FatJet_phi, fRVec FatJet_mass,
    fRVec FatJet_msoftdrop, fRVec FatJet_particleNet_XbbVsQCD,
    float dau1_pt, float dau1_eta, float dau1_phi, float dau1_mass,
    float dau2_pt, float dau2_eta, float dau2_phi, float dau2_mass,
    float met_pt, float met_phi)
{
  std::vector<float> all_HHbtag_scores;
  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    all_HHbtag_scores.push_back(-999.);
  }

  int bjet1_idx = -1;
  int bjet2_idx = -1;
  int vbfjet1_idx = -1;
  int vbfjet2_idx = -1;
  int isBoosted_ = 0;
  int fatjet_idx = -1;
  int fatjet2jet1_idx = -1;
  int fatjet2jet2_idx = -1;
  std::vector <int> ctjet_indexes, fwjet_indexes;

  if (pairType < 0) {
    return output({all_HHbtag_scores, bjet1_idx, bjet2_idx, vbfjet1_idx, vbfjet2_idx,
      ctjet_indexes, fwjet_indexes, isBoosted_, fatjet_idx});
  }

  auto dau1_tlv = TLorentzVector();
  auto dau2_tlv = TLorentzVector();
  auto met_tlv = TLorentzVector();
  dau1_tlv.SetPtEtaPhiM(dau1_pt, dau1_eta, dau1_phi, dau1_mass);
  dau2_tlv.SetPtEtaPhiM(dau2_pt, dau2_eta, dau2_phi, dau2_mass);
  met_tlv.SetPxPyPzE(met_pt * cos(met_phi), met_pt * sin(met_phi), 0, met_pt);

  std::vector <jet_idx_btag> jet_indexes;
  std::vector <int> all_jet_indexes;
  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    if ((Jet_puId[ijet] < 1 && Jet_pt[ijet] <= 50) || Jet_jetId[ijet] < 2)
      continue;
    auto jet_tlv = TLorentzVector();
    jet_tlv.SetPtEtaPhiM(Jet_pt[ijet], Jet_eta[ijet], Jet_phi[ijet], Jet_mass[ijet]);
    if (jet_tlv.DeltaR(dau1_tlv) < 0.5 || jet_tlv.DeltaR(dau2_tlv) < 0.5)
      continue;
    if (Jet_pt[ijet] > 20 && fabs(Jet_eta[ijet]) < max_bjet_eta)
      jet_indexes.push_back(jet_idx_btag({(int) ijet, Jet_btag[ijet]}));
    if (Jet_pt[ijet] > 20 && fabs(Jet_eta[ijet]) < max_vbfjet_eta)
      all_jet_indexes.push_back(ijet);
  }

  // new definiton of boosted only requiring one AK8 jet (no subjets match)
  std::vector <jet_idx_btag> fatjet_indexes;
  for (size_t ifatjet = 0; ifatjet < FatJet_pt.size(); ifatjet++) {
    auto fatjet_tlv = TLorentzVector();
    fatjet_tlv.SetPtEtaPhiM(FatJet_pt[ifatjet], FatJet_eta[ifatjet],
      FatJet_phi[ifatjet], FatJet_mass[ifatjet]);
    if (fatjet_tlv.Pt() < 250 || fabs(fatjet_tlv.Eta()) > 2.5) continue;
    if (fatjet_tlv.DeltaR(dau1_tlv) < 0.8) continue;
    if (fatjet_tlv.DeltaR(dau2_tlv) < 0.8) continue;
    if (FatJet_msoftdrop.at(ifatjet) < 30) continue;
    fatjet_indexes.push_back(jet_idx_btag({(int) ifatjet, FatJet_particleNet_XbbVsQCD.at(ifatjet)}));
  }
  if (fatjet_indexes.size() != 0) {
    isBoosted_ = 1;
    std::stable_sort(fatjet_indexes.begin(), fatjet_indexes.end(), jetSort);
    fatjet_idx = fatjet_indexes[0].idx;

    TLorentzVector fatjet_tlv = TLorentzVector();
    fatjet_tlv.SetPtEtaPhiM(FatJet_pt[fatjet_idx], FatJet_eta[fatjet_idx],
                            FatJet_phi[fatjet_idx], FatJet_mass[fatjet_idx]);

    for (size_t ijet = 0; ijet < all_jet_indexes.size(); ijet++) {
      auto jet_index = all_jet_indexes[ijet];
      auto jet_tlv = TLorentzVector();
      jet_tlv.SetPtEtaPhiM(Jet_pt[jet_index], Jet_eta[jet_index],
                           Jet_phi[jet_index], Jet_mass[jet_index]);
      
      if (jet_tlv.DeltaR(fatjet_tlv) < 0.8) {
        if (fatjet2jet1_idx == -1)
          fatjet2jet1_idx = jet_index;
        else if (fatjet2jet2_idx == -1)
          fatjet2jet2_idx = jet_index;
        else
          break; // jets are order in pt by construction
      }
    }
  }

  if (jet_indexes.size() >= 2 && !isBoosted_) {
    std::stable_sort(jet_indexes.begin(), jet_indexes.end(), jetSort);

    auto htt_tlv = dau1_tlv + dau2_tlv;
    std::vector <float> HHbtag_jet_pt_, HHbtag_jet_eta_, HHbtag_rel_jet_M_pt_, HHbtag_rel_jet_E_pt_;
    std::vector <float> HHbtag_jet_htt_deta_, HHbtag_jet_htt_dphi_, HHbtag_jet_btagOutput_;

    for (auto &jet : jet_indexes) {
      auto jet_tlv = TLorentzVector();
      jet_tlv.SetPtEtaPhiM(Jet_pt[jet.idx], Jet_eta[jet.idx], Jet_phi[jet.idx], Jet_mass[jet.idx]);
      HHbtag_jet_pt_.push_back(jet_tlv.Pt());
      HHbtag_jet_eta_.push_back(jet_tlv.Eta());
      HHbtag_rel_jet_M_pt_.push_back(jet_tlv.M() / jet_tlv.Pt());
      HHbtag_rel_jet_E_pt_.push_back(jet_tlv.E() / jet_tlv.Pt());
      HHbtag_jet_htt_deta_.push_back(htt_tlv.Eta() - jet_tlv.Eta());
      HHbtag_jet_htt_dphi_.push_back(ROOT::Math::VectorUtil::DeltaPhi(htt_tlv, jet_tlv));
      HHbtag_jet_btagOutput_.push_back(jet.btag);
    }

    auto HHbtag_htt_met_dphi_ = (float) ROOT::Math::VectorUtil::DeltaPhi(htt_tlv, met_tlv);
    auto HHbtag_htt_scalar_pt_ = (float) (dau1_tlv.Pt() + dau2_tlv.Pt());
    auto HHbtag_rel_met_pt_htt_pt_ = (float) met_tlv.Pt() / HHbtag_htt_scalar_pt_;
    auto HHbtag_htt_pt_ = (float) htt_tlv.Pt();
    auto HHbtag_htt_eta_ = (float) htt_tlv.Eta();
    int HHbtag_channel_ = -1;

    if (pairType == 0 || pairType == 1 || pairType == 2)
      HHbtag_channel_ = pairType;
    // else if ee/mumu/emu -> will be included in next HHBTag training

    auto HHbtag_scores = HHbtagger_.GetScore(HHbtag_jet_pt_, HHbtag_jet_eta_,
      HHbtag_rel_jet_M_pt_, HHbtag_rel_jet_E_pt_, HHbtag_jet_htt_deta_,
      HHbtag_jet_btagOutput_, HHbtag_jet_htt_dphi_, year_, HHbtag_channel_,
      HHbtag_htt_pt_, HHbtag_htt_eta_, HHbtag_htt_met_dphi_,
      HHbtag_rel_met_pt_htt_pt_, HHbtag_htt_scalar_pt_, event);

    for (size_t ijet = 0; ijet < jet_indexes.size(); ijet++) {
      all_HHbtag_scores[jet_indexes[ijet].idx] = HHbtag_scores[ijet];
    }
    std::vector <jet_idx_btag> jet_indexes_hhbtag;
    for (size_t ijet = 0; ijet < jet_indexes.size(); ijet++) {
      jet_indexes_hhbtag.push_back(jet_idx_btag({jet_indexes[ijet].idx, HHbtag_scores[ijet]}));
    }
    std::stable_sort(jet_indexes_hhbtag.begin(), jet_indexes_hhbtag.end(), jetSort);
    bjet1_idx = jet_indexes_hhbtag[0].idx;
    bjet2_idx = jet_indexes_hhbtag[1].idx;

    if (Jet_pt[bjet1_idx] < Jet_pt[bjet2_idx]) {
      auto aux = bjet1_idx;
      bjet1_idx = bjet2_idx;
      bjet2_idx = aux;
    }
  }

  if (all_jet_indexes.size() >= 2) { // 0 fatjet2jet + 0 bjets + 2 vbf jets
    std::vector <jet_pair_mass> vbfjet_indexes;
    for (size_t ijet = 0; ijet < all_jet_indexes.size(); ijet++) {
      auto jet1_index = all_jet_indexes[ijet];
      auto jet1_tlv = TLorentzVector();
      jet1_tlv.SetPtEtaPhiM(Jet_pt[jet1_index], Jet_eta[jet1_index],
                            Jet_phi[jet1_index], Jet_mass[jet1_index]);
      
      if (isBoosted_ && (jet1_index == fatjet2jet1_idx || jet1_index == fatjet2jet2_idx))
        continue;
      if (!isBoosted_ && (jet1_index == (int) bjet1_idx || jet1_index == (int) bjet2_idx))
        continue;
      
      for (size_t jjet = ijet + 1; jjet < all_jet_indexes.size(); jjet++) {
        auto jet2_index = all_jet_indexes[jjet];
        auto jet2_tlv = TLorentzVector();
        jet2_tlv.SetPtEtaPhiM(Jet_pt[jet2_index], Jet_eta[jet2_index],
                              Jet_phi[jet2_index], Jet_mass[jet2_index]);
        
        if (isBoosted_ && (jet2_index == fatjet2jet1_idx || jet2_index == fatjet2jet2_idx))
          continue;
        if (!isBoosted_ && (jet2_index == (int) bjet1_idx || jet2_index == (int) bjet2_idx))
          continue;
        if (Jet_pt[jet1_index] < 30 || Jet_pt[jet2_index] < 30)
          continue;
        
        auto jj_tlv = jet1_tlv + jet2_tlv;
        vbfjet_indexes.push_back(jet_pair_mass({jet1_index, jet2_index, (float) jj_tlv.M()}));
      }
    }
    if (vbfjet_indexes.size() > 0) {
      std::stable_sort(vbfjet_indexes.begin(), vbfjet_indexes.end(), jetPairSort);
      vbfjet1_idx = vbfjet_indexes[0].idx1;
      vbfjet2_idx = vbfjet_indexes[0].idx2;

      if (Jet_pt[vbfjet1_idx] < Jet_pt[vbfjet2_idx]) {
        auto aux = vbfjet1_idx;
        vbfjet1_idx = vbfjet2_idx;
        vbfjet2_idx = aux;
      }
    }
  }

  // additional central and forward jets
  for (auto & ijet : all_jet_indexes) {
    if ((int) ijet == bjet1_idx || (int) ijet == bjet2_idx
        || (int) ijet == vbfjet1_idx || (int) ijet == vbfjet2_idx
        || (int) ijet == fatjet2jet1_idx || (int) ijet == fatjet2jet2_idx)
      continue;
    if (fabs(Jet_eta[ijet]) < max_bjet_eta)
      ctjet_indexes.push_back(ijet);
    else if (fabs(Jet_eta[ijet]) < max_vbfjet_eta && Jet_pt[ijet] > 30)
      fwjet_indexes.push_back(ijet);
  }

  return output({all_HHbtag_scores, bjet1_idx, bjet2_idx, vbfjet1_idx, vbfjet2_idx,
    ctjet_indexes, fwjet_indexes, isBoosted_, fatjet_idx});
}

// definition compatible with Run-3 analyses using PUPPI jets and don't need Jet_puId
// this definition uses the new fatjet without subjet apporoach
output HHJetsInterface::GetHHJets(
    unsigned long long int event, int pairType,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
    fRVec Jet_jetId, fRVec Jet_btag,
    fRVec FatJet_pt, fRVec FatJet_eta, fRVec FatJet_phi, fRVec FatJet_mass,
    fRVec FatJet_msoftdrop, fRVec FatJet_particleNet_XbbVsQCD,
    float dau1_pt, float dau1_eta, float dau1_phi, float dau1_mass,
    float dau2_pt, float dau2_eta, float dau2_phi, float dau2_mass,
    float met_pt, float met_phi,
    bool isTauTauJetTrigger, bool isQuadJetTrigger, bool isVBFtrigger,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi,
    std::vector<trig_req> hlt_ditaujet_jetsRequest, std::vector<trig_req> hlt_quadjet_jetsRequest,
    std::vector<trig_req> hlt_vbf_jetsRequest)
{
  std::vector<float> all_HHbtag_scores;
  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    all_HHbtag_scores.push_back(-999.);
  }

  int bjet1_idx = -1;
  int bjet2_idx = -1;
  int vbfjet1_idx = -1;
  int vbfjet2_idx = -1;
  int isBoosted_ = 0;
  int fatjet_idx = -1;
  int fatjet2jet1_idx = -1;
  int fatjet2jet2_idx = -1;
  std::vector <int> ctjet_indexes, fwjet_indexes;

  if (pairType < 0) {
    return output({all_HHbtag_scores, bjet1_idx, bjet2_idx, vbfjet1_idx, vbfjet2_idx,
      ctjet_indexes, fwjet_indexes, isBoosted_, fatjet_idx});
  }

  auto dau1_tlv = TLorentzVector();
  auto dau2_tlv = TLorentzVector();
  auto met_tlv = TLorentzVector();
  dau1_tlv.SetPtEtaPhiM(dau1_pt, dau1_eta, dau1_phi, dau1_mass);
  dau2_tlv.SetPtEtaPhiM(dau2_pt, dau2_eta, dau2_phi, dau2_mass);
  met_tlv.SetPxPyPzE(met_pt * cos(met_phi), met_pt * sin(met_phi), 0, met_pt);

  std::vector <jet_idx_btag> jet_indexes;
  std::vector <int> all_jet_indexes;
  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    if (Jet_jetId[ijet] < 2)
      continue;
    auto jet_tlv = TLorentzVector();
    jet_tlv.SetPtEtaPhiM(Jet_pt[ijet], Jet_eta[ijet], Jet_phi[ijet], Jet_mass[ijet]);
    if (jet_tlv.DeltaR(dau1_tlv) < 0.5 || jet_tlv.DeltaR(dau2_tlv) < 0.5)
      continue;
    if (Jet_pt[ijet] > 20 && fabs(Jet_eta[ijet]) < max_bjet_eta)
      jet_indexes.push_back(jet_idx_btag({(int) ijet, Jet_btag[ijet]}));
    if (Jet_pt[ijet] > 20 && fabs(Jet_eta[ijet]) < max_vbfjet_eta)
      all_jet_indexes.push_back(ijet);
  }

  // new definiton of boosted only requiring one AK8 jet (no subjets match)
  std::vector <jet_idx_btag> fatjet_indexes;
  for (size_t ifatjet = 0; ifatjet < FatJet_pt.size(); ifatjet++) {
    auto fatjet_tlv = TLorentzVector();
    fatjet_tlv.SetPtEtaPhiM(FatJet_pt[ifatjet], FatJet_eta[ifatjet],
      FatJet_phi[ifatjet], FatJet_mass[ifatjet]);
    if (fatjet_tlv.Pt() < 250 || fabs(fatjet_tlv.Eta()) > 2.5) continue;
    if (fatjet_tlv.DeltaR(dau1_tlv) < 0.8) continue;
    if (fatjet_tlv.DeltaR(dau2_tlv) < 0.8) continue;
    if (FatJet_msoftdrop.at(ifatjet) < 30) continue;
    fatjet_indexes.push_back(jet_idx_btag({(int) ifatjet, FatJet_particleNet_XbbVsQCD.at(ifatjet)}));
  }
  if (fatjet_indexes.size() != 0) {
    isBoosted_ = 1;
    std::stable_sort(fatjet_indexes.begin(), fatjet_indexes.end(), jetSort);
    fatjet_idx = fatjet_indexes[0].idx;

    TLorentzVector fatjet_tlv = TLorentzVector();
    fatjet_tlv.SetPtEtaPhiM(FatJet_pt[fatjet_idx], FatJet_eta[fatjet_idx],
                            FatJet_phi[fatjet_idx], FatJet_mass[fatjet_idx]);

    for (size_t ijet = 0; ijet < all_jet_indexes.size(); ijet++) {
      auto jet_index = all_jet_indexes[ijet];
      auto jet_tlv = TLorentzVector();
      jet_tlv.SetPtEtaPhiM(Jet_pt[jet_index], Jet_eta[jet_index],
                           Jet_phi[jet_index], Jet_mass[jet_index]);
      
      if (jet_tlv.DeltaR(fatjet_tlv) < 0.8) {
        if (fatjet2jet1_idx == -1)
          fatjet2jet1_idx = jet_index;
        else if (fatjet2jet2_idx == -1)
          fatjet2jet2_idx = jet_index;
        else
          break; // jets are order in pt by construction
      }
    }
  }

  if (jet_indexes.size() >= 2 && !isBoosted_) {
    std::stable_sort(jet_indexes.begin(), jet_indexes.end(), jetSort);

    auto htt_tlv = dau1_tlv + dau2_tlv;
    std::vector <float> HHbtag_jet_pt_, HHbtag_jet_eta_, HHbtag_rel_jet_M_pt_, HHbtag_rel_jet_E_pt_;
    std::vector <float> HHbtag_jet_htt_deta_, HHbtag_jet_htt_dphi_, HHbtag_jet_btagOutput_;

    for (auto &jet : jet_indexes) {
      auto jet_tlv = TLorentzVector();
      jet_tlv.SetPtEtaPhiM(Jet_pt[jet.idx], Jet_eta[jet.idx], Jet_phi[jet.idx], Jet_mass[jet.idx]);
      HHbtag_jet_pt_.push_back(jet_tlv.Pt());
      HHbtag_jet_eta_.push_back(jet_tlv.Eta());
      HHbtag_rel_jet_M_pt_.push_back(jet_tlv.M() / jet_tlv.Pt());
      HHbtag_rel_jet_E_pt_.push_back(jet_tlv.E() / jet_tlv.Pt());
      HHbtag_jet_htt_deta_.push_back(htt_tlv.Eta() - jet_tlv.Eta());
      HHbtag_jet_htt_dphi_.push_back(ROOT::Math::VectorUtil::DeltaPhi(htt_tlv, jet_tlv));
      HHbtag_jet_btagOutput_.push_back(jet.btag);
    }

    auto HHbtag_htt_met_dphi_ = (float) ROOT::Math::VectorUtil::DeltaPhi(htt_tlv, met_tlv);
    auto HHbtag_htt_scalar_pt_ = (float) (dau1_tlv.Pt() + dau2_tlv.Pt());
    auto HHbtag_rel_met_pt_htt_pt_ = (float) met_tlv.Pt() / HHbtag_htt_scalar_pt_;
    auto HHbtag_htt_pt_ = (float) htt_tlv.Pt();
    auto HHbtag_htt_eta_ = (float) htt_tlv.Eta();
    int HHbtag_channel_ = -1;

    if (pairType == 0 || pairType == 1 || pairType == 2)
      HHbtag_channel_ = pairType;
    // else if ee/mumu/emu -> will be included in next HHBTag training

    auto HHbtag_scores = HHbtagger_.GetScore(HHbtag_jet_pt_, HHbtag_jet_eta_,
      HHbtag_rel_jet_M_pt_, HHbtag_rel_jet_E_pt_, HHbtag_jet_htt_deta_,
      HHbtag_jet_btagOutput_, HHbtag_jet_htt_dphi_, year_, HHbtag_channel_,
      HHbtag_htt_pt_, HHbtag_htt_eta_, HHbtag_htt_met_dphi_,
      HHbtag_rel_met_pt_htt_pt_, HHbtag_htt_scalar_pt_, event);

    for (size_t ijet = 0; ijet < jet_indexes.size(); ijet++) {
      all_HHbtag_scores[jet_indexes[ijet].idx] = HHbtag_scores[ijet];
    }
    std::vector <jet_idx_btag> jet_indexes_hhbtag;
    for (size_t ijet = 0; ijet < jet_indexes.size(); ijet++) {
      jet_indexes_hhbtag.push_back(jet_idx_btag({jet_indexes[ijet].idx, HHbtag_scores[ijet]}));
    }

    if (!bypass_HHBTag_) {
      std::stable_sort(jet_indexes_hhbtag.begin(), jet_indexes_hhbtag.end(), jetSort);
      bjet1_idx = jet_indexes_hhbtag[0].idx;
      bjet2_idx = jet_indexes_hhbtag[1].idx;

      if (Jet_pt[bjet1_idx] < Jet_pt[bjet2_idx]) {
        auto aux = bjet1_idx;
        bjet1_idx = bjet2_idx;
        bjet2_idx = aux;
      }
    }
    else {
      bjet1_idx = jet_indexes[0].idx;
      bjet2_idx = jet_indexes[1].idx;
      
      if (Jet_pt[bjet1_idx] < Jet_pt[bjet2_idx]) {
        auto aux = bjet1_idx;
        bjet1_idx = bjet2_idx;
        bjet2_idx = aux;
      }
    }
  }

  if (all_jet_indexes.size() >= 2) { // 0 fatjet2jet + 0 bjets + 2 vbf jets
    std::vector <jet_pair_mass> vbfjet_indexes;
    for (size_t ijet = 0; ijet < all_jet_indexes.size(); ijet++) {
      auto jet1_index = all_jet_indexes[ijet];
      auto jet1_tlv = TLorentzVector();
      jet1_tlv.SetPtEtaPhiM(Jet_pt[jet1_index], Jet_eta[jet1_index],
                            Jet_phi[jet1_index], Jet_mass[jet1_index]);
      
      if (isBoosted_ && (jet1_index == fatjet2jet1_idx || jet1_index == fatjet2jet2_idx))
        continue;
      if (!isBoosted_ && (jet1_index == (int) bjet1_idx || jet1_index == (int) bjet2_idx))
        continue;
      
      for (size_t jjet = ijet + 1; jjet < all_jet_indexes.size(); jjet++) {
        auto jet2_index = all_jet_indexes[jjet];
        auto jet2_tlv = TLorentzVector();
        jet2_tlv.SetPtEtaPhiM(Jet_pt[jet2_index], Jet_eta[jet2_index],
                              Jet_phi[jet2_index], Jet_mass[jet2_index]);
        
        if (isBoosted_ && (jet2_index == fatjet2jet1_idx || jet2_index == fatjet2jet2_idx))
          continue;
        if (!isBoosted_ && (jet2_index == (int) bjet1_idx || jet2_index == (int) bjet2_idx))
          continue;
        if (Jet_pt[jet1_index] < 30 || Jet_pt[jet2_index] < 30)
          continue;
        
        auto jj_tlv = jet1_tlv + jet2_tlv;
        vbfjet_indexes.push_back(jet_pair_mass({jet1_index, jet2_index, (float) jj_tlv.M()}));
      }
    }
    if (vbfjet_indexes.size() > 0) {
      std::stable_sort(vbfjet_indexes.begin(), vbfjet_indexes.end(), jetPairSort);
      vbfjet1_idx = vbfjet_indexes[0].idx1;
      vbfjet2_idx = vbfjet_indexes[0].idx2;

      if (Jet_pt[vbfjet1_idx] < Jet_pt[vbfjet2_idx]) {
        auto aux = vbfjet1_idx;
        vbfjet1_idx = vbfjet2_idx;
        vbfjet2_idx = aux;
      }
    }
  }

  // additional central and forward jets
  for (auto & ijet : all_jet_indexes) {
    if ((int) ijet == bjet1_idx || (int) ijet == bjet2_idx
        || (int) ijet == vbfjet1_idx || (int) ijet == vbfjet2_idx
        || (int) ijet == fatjet2jet1_idx || (int) ijet == fatjet2jet2_idx)
      continue;
    if (fabs(Jet_eta[ijet]) < max_bjet_eta)
      ctjet_indexes.push_back(ijet);
    else if (fabs(Jet_eta[ijet]) < max_vbfjet_eta && Jet_pt[ijet] > 30)
      fwjet_indexes.push_back(ijet);
  }

  return output({all_HHbtag_scores, bjet1_idx, bjet2_idx, vbfjet1_idx, vbfjet2_idx,
    ctjet_indexes, fwjet_indexes, isBoosted_, fatjet_idx});
}

// this definition uses the new fatjet without subjet apporoach
output HHJetsInterface::GetHHJetsWrapper (
  unsigned long long int event, bool useCHSjets,
  fRVec jetpt, fRVec jeteta, fRVec jetphi, fRVec jetmass,
  iRVec jetpuId, fRVec jetjetId, fRVec jetbtag,
  fRVec fatjetpt, fRVec fatjeteta, fRVec fatjetphi, fRVec fatjetmass,
  fRVec fatjetmsoftdrop, fRVec fatjetpnetxbbvsqcd,
  int pairType, int dau1_index, int dau2_index,
  fRVec muonpt, fRVec muoneta, fRVec muonphi, fRVec muonmass,
  fRVec elept, fRVec eleeta, fRVec elephi, fRVec elemass,
  fRVec taupt, fRVec taueta, fRVec tauphi, fRVec taumass,
  float met_pt, float met_phi,
  bool isTauTauJetTrigger, bool isQuadJetTrigger, bool isVBFtrigger,
  iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi,
  std::vector<trig_req> hlt_ditaujet_jetsRequest, std::vector<trig_req> hlt_quadjet_jetsRequest,
  std::vector<trig_req> hlt_vbf_jetsRequest)
{
  leps lptns = GetLeps(muonpt, muoneta, muonphi, muonmass,
                       elept, eleeta, elephi, elemass,
                       taupt, taueta, tauphi, taumass,
                       dau1_index, dau2_index, pairType);

  if (useCHSjets) {
    return GetHHJets(event, pairType,
      jetpt, jeteta, jetphi, jetmass,
      jetpuId, jetjetId, jetbtag,
      fatjetpt, fatjeteta, fatjetphi, fatjetmass,
      fatjetmsoftdrop, fatjetpnetxbbvsqcd,
      lptns.dau1_pt, lptns.dau1_eta, lptns.dau1_phi, lptns.dau1_mass,
      lptns.dau2_pt, lptns.dau2_eta, lptns.dau2_phi, lptns.dau2_mass,
      met_pt, met_phi);
  }
  else {
    return GetHHJets(event, pairType,
      jetpt, jeteta, jetphi, jetmass,
      jetjetId, jetbtag,
      fatjetpt, fatjeteta, fatjetphi, fatjetmass,
      fatjetmsoftdrop, fatjetpnetxbbvsqcd,
      lptns.dau1_pt, lptns.dau1_eta, lptns.dau1_phi, lptns.dau1_mass,
      lptns.dau2_pt, lptns.dau2_eta, lptns.dau2_phi, lptns.dau2_mass,
      met_pt, met_phi,
      isTauTauJetTrigger, isQuadJetTrigger, isVBFtrigger,
      TrigObj_id, TrigObj_filterBits, TrigObj_eta, TrigObj_phi,
      hlt_ditaujet_jetsRequest, hlt_quadjet_jetsRequest,
      hlt_vbf_jetsRequest);
  }
}

// GetScore
std::vector<float> HHJetsInterface::GetScore(
  std::vector<float> HHbtag_jet_pt_, std::vector<float> HHbtag_jet_eta_, std::vector<float> HHbtag_rel_jet_M_pt_,
  std::vector<float> HHbtag_rel_jet_E_pt_, std::vector<float> HHbtag_jet_htt_deta_, std::vector<float> HHbtag_jet_btagOutput_,
  std::vector<float> HHbtag_jet_htt_dphi_, int HHbtag_year_, int HHbtag_channel_, float HHbtag_tauH_pt_, float HHbtag_tauH_eta_,
  float HHbtag_htt_met_dphi_, float HHbtag_rel_met_pt_htt_pt_, float HHbtag_htt_scalar_pt_, unsigned long long int HHbtag_evt_)
{
  // Get HHbtag score
  auto HHbtag_scores = HHbtagger_.GetScore(HHbtag_jet_pt_, HHbtag_jet_eta_, HHbtag_rel_jet_M_pt_,
      HHbtag_rel_jet_E_pt_, HHbtag_jet_htt_deta_, HHbtag_jet_btagOutput_, HHbtag_jet_htt_dphi_,
      HHbtag_year_, HHbtag_channel_, HHbtag_tauH_pt_, HHbtag_tauH_eta_, HHbtag_htt_met_dphi_,
      HHbtag_rel_met_pt_htt_pt_, HHbtag_htt_scalar_pt_, HHbtag_evt_);

  // Store HHbtag scores in a map<jet_idx,HHbtag_score>
  std::vector<float> jets_and_HHbtag;
  for (unsigned int i = 0; i < HHbtag_jet_pt_.size(); i++)
  {
    jets_and_HHbtag.push_back(HHbtag_scores.at(i));
  }

  return jets_and_HHbtag;
}
