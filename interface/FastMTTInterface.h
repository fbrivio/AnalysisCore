#ifndef FastMTTInterface_h
#define FastMTTInterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class FastMTTInterface                                                                                       //
//                                                                                                                //
//   Class to compute FastMTT. Modified from                                                                      //
//                                                                                                                //
//   Original SVfit package from:                                                                                 //
//    - https://github.com/SVfit/ClassicSVfit                                                                     //
//                                                                                                                //
//   Author: Francesco Brivio (Milano-Bicocca)                                                                    //
//   Date  : October 2024                                                                                         //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <vector>
#include <string>
#include <cmath>
#include <chrono>

// ROOT libraries
#include <TLorentzVector.h>
#include "TMatrixD.h"

// FastMTT and ClassicSVfit libraries
#include "TauAnalysis/ClassicSVfit/interface/FastMTT.h"
//#include "TauAnalysis/ClassicSVfit/interface/ClassicSVfitIntegrand.h"
#include "TauAnalysis/ClassicSVfit/interface/MeasuredTauLepton.h"
#include "TauAnalysis/ClassicSVfit/interface/svFitHistogramAdapter.h"

using namespace classic_svFit;

// FastMTTInterface class
class FastMTTInterface {

  public:
  FastMTTInterface ();
  ~FastMTTInterface ();
  std::vector<double> FitAndGetResultWithInputs(
    int verbosity, int pairType, int DM1, int DM2,
    double tau1_pt, double tau1_eta, double tau1_phi, double tau1_mass,
    double tau2_pt, double tau2_eta, double tau2_phi, double tau2_mass,
    double met_pt, double met_phi,
    double met_covXX, double met_covXY, double met_covYY);

};

#endif // FastMTTInterface_h
