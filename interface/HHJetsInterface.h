#ifndef HHJetsInterface_h
#define HHJetsInterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class HHJetsInterface                                                                                        //
//                                                                                                                //
//   Class to compute HHbtag output.                                                                              //
//                                                                                                                //
//   Author: Jaime León Holgado                                                                                   //
//   Date  : June 2021                                                                                            //
//                                                                                                                //
//   modified from                                                                                                //
//                                                                                                                //
//   https://github.com/LLRCMS/KLUBAnalysis/blob/VBF_legacy/interface/HHbtagKLUBinterface.h                       //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

// HHbtag libraries
#include "HHTools/HHbtag/interface/HH_BTag.h"

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>
#include <Math/VectorUtil.h>

// CMSSW
#include "DataFormats/Math/interface/deltaPhi.h"

// Utils
#include "Tools/Tools/interface/HHutils.h"

// HHJetsInterface class
class HHJetsInterface {

  public:
    HHJetsInterface (std::string model_0, std::string model_1, int year, bool extEtaAcc,
                     bool bypass_HHBTag, float btag_algo_cut, bool applyTrg, bool applyOfln);
    ~HHJetsInterface ();

  // definition compatible with Run-2 analyses using CHS jets and need Jet_puId
  // this definition uses the new fatjet without subjet apporoach
  output GetHHJets(unsigned long long int event, int pairType,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
    iRVec Jet_puId, fRVec Jet_jetId, fRVec Jet_btag,
    fRVec FatJet_pt, fRVec FatJet_eta, fRVec FatJet_phi, fRVec FatJet_mass,
    fRVec FatJet_msoftdrop, fRVec FatJet_particleNet_XbbVsQCD,
    float dau1_pt, float dau1_eta, float dau1_phi, float dau1_mass,
    float dau2_pt, float dau2_eta, float dau2_phi, float dau2_mass,
    float met_pt, float met_phi);

  // defintiion compatible with Run-3 analyses using PUPPI jets and don't need Jet_puId
  // this definition uses the new fatjet without subjet apporoach
  output GetHHJets(unsigned long long int event, int pairType,
    fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
    fRVec Jet_jetId, fRVec Jet_btag,
    fRVec FatJet_pt, fRVec FatJet_eta, fRVec FatJet_phi, fRVec FatJet_mass,
    fRVec FatJet_msoftdrop, fRVec FatJet_particleNet_XbbVsQCD,
    float dau1_pt, float dau1_eta, float dau1_phi, float dau1_mass,
    float dau2_pt, float dau2_eta, float dau2_phi, float dau2_mass,
    float met_pt, float met_phi,
    bool isTauTauJetTrigger, bool isQuadJetTrigger, bool isVBFtrigger,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi,
    std::vector<trig_req> hlt_ditaujet_jetsRequest, std::vector<trig_req> hlt_quadjet_jetsRequest,
    std::vector<trig_req> hlt_vbf_jetsRequest);
  
  // this definition uses the new fatjet without subjet apporoach
  output GetHHJetsWrapper (unsigned long long int event, bool useCHSjets,
    fRVec jetpt, fRVec jeteta, fRVec jetphi, fRVec jetmass,
    iRVec jetpuId, fRVec jetjetId, fRVec jetbtag,
    fRVec fatjetpt, fRVec fatjeteta, fRVec fatjetphi, fRVec fatjetmass,
    fRVec fatjetmsoftdrop, fRVec fatjetpnetxbbvsqcd,
    int pairType, int dau1_index, int dau2_index,
    fRVec muonpt, fRVec muoneta, fRVec muonphi, fRVec muonmass,
    fRVec elept, fRVec eleeta, fRVec elephi, fRVec elemass,
    fRVec taupt, fRVec taueta, fRVec tauphi, fRVec taumass,
    float met_pt, float met_phi,
    bool isTauTauJetTrigger, bool isQuadJetTrigger, bool isVBFtrigger,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi,
    std::vector<trig_req> hlt_ditaujet_jetsRequest, std::vector<trig_req> hlt_quadjet_jetsRequest,
    std::vector<trig_req> hlt_vbf_jetsRequest);

  std::vector<float> GetScore(
    std::vector<float> HHbtag_jet_pt_, std::vector<float> HHbtag_jet_eta_, std::vector<float> HHbtag_rel_jet_M_pt_,
    std::vector<float> HHbtag_rel_jet_E_pt_, std::vector<float> HHbtag_jet_htt_deta_, std::vector<float> HHbtag_jet_btagOutput_,
    std::vector<float> HHbtag_jet_htt_dphi_, int HHbtag_year_, int HHbtag_channel_, float HHbtag_tauH_pt_, float HHbtag_tauH_eta_,
    float HHbtag_htt_met_dphi_, float HHbtag_rel_met_pt_htt_pt_, float HHbtag_htt_scalar_pt_, unsigned long long int HHbtag_evt_);

  private:
    hh_btag::HH_BTag HHbtagger_;
    int year_;
    float max_bjet_eta = 2.4;
    float max_vbfjet_eta = 4.7;
    bool bypass_HHBTag_;
    float btag_algo_cut_;

    bool applyTrg_;
    bool applyOfln_;

    leps GetLeps(fRVec muonpt, fRVec muoneta, fRVec muonphi, fRVec muonmass,
    fRVec electronpt, fRVec electroneta, fRVec electronphi, fRVec electronmass,
    fRVec taupt, fRVec taueta, fRVec tauphi, fRVec taumass,
    int dau1_index, int dau2_index, int pairType);
};

#endif // HHJetsInterface
