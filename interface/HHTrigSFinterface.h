#ifndef HHTrigSFInterface_h
#define HHTrigSFInterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class HHTrigSFInterface                                                                                      //
//                                                                                                                //
//   Class to compute HH trigger scale factors.                                                                   //
//                                                                                                                //
//   Author: Jaime León Holgado & Jona Motta                                                                      //
//   Date  : Feb 2022 & Apr 2024                                                                                  //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>


// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>
#include <Math/VectorUtil.h>
#include <TH3.h>
#include <TFile.h>

// CMSSW
#include "HTT-utilities/LepEffInterface/interface/ScaleFactor.h"
#include "TauAnalysisTools/TauTriggerSFs/interface/SFProvider.h"

typedef ROOT::VecOps::RVec<float> fRVec;
typedef ROOT::VecOps::RVec<bool> bRVec;
typedef ROOT::VecOps::RVec<int> iRVec;


// HHTrigSFInterface class
class HHTrigSFinterface {
  
  public:
    HHTrigSFinterface (
      int year, float mutau_pt_th1, float mutau_pt_th2, float etau_pt_th1, float etau_pt_th2,
      std::string eTrgSF_file, std::string eTauTrgSF_file, std::string muTrgSF_file,
      std::string muTauTrgSF_file, std::string tauTrgSF_ditau_file, std::string tauTrgSF_mutau_file,
      std::string tauTrgSF_etau_file, std::string tauTrgSF_vbf_file, std::string jetTrgSF_vbf_file);    
    ~HHTrigSFinterface ();

    std::vector<double> get_scale_factors(int pairType, int isVBFtrigger,
      int dau1_decayMode, float dau1_pt, float dau1_eta,
      int dau2_decayMode, float dau2_pt, float dau2_eta,
      float vbfjet1_pt, float vbfjet1_eta, float vbfjet1_phi, float vbfjet1_mass,
      float vbfjet2_pt, float vbfjet2_eta, float vbfjet2_phi, float vbfjet2_mass);

    std::vector<double> get_hh_trigsf(int pairType, int isVBFtrigger,
      int dau1_index, int dau2_index, int vbfjet1_index, int vbfjet2_index,
      fRVec muon_pt, fRVec muon_eta, fRVec electron_pt, fRVec electron_eta,
      fRVec tau_pt, fRVec tau_eta, iRVec tau_decayMode,
      fRVec jet_pt, fRVec jet_eta, fRVec jet_phi, fRVec jet_mass);
  
    private:
      std::vector <int> decayModes = {0, 1, 10, 11};

      int year_;
      float mutau_pt_th1_;
      float mutau_pt_th2_;
      float etau_pt_th1_;
      float etau_pt_th2_;
      ScaleFactor eTrgSF = ScaleFactor();
      ScaleFactor eTauTrgSF = ScaleFactor();
      ScaleFactor muTrgSF = ScaleFactor();
      ScaleFactor muTauTrgSF;
      tau_trigger::SFProvider tauTrgSF_ditau;
      tau_trigger::SFProvider tauTrgSF_mutau;
      tau_trigger::SFProvider tauTrgSF_etau;
      tau_trigger::SFProvider tauTrgSF_vbf;
      TH3F* jetTrgSF_vbf;
      double getContentHisto3D(TH3F* TH3, double x, double y, double z, bool unc);
};

#endif // HHTrigSFinterface
