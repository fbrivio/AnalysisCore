#ifndef HHutils_h
#define HHutils_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class HHutils                                                                                                //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>

// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>
#include <Math/VectorUtil.h>

// CMSSW
#include "DataFormats/Math/interface/deltaPhi.h"

//------------------------------------------------------------------------
// general types used everywhere
typedef ROOT::VecOps::RVec<float> fRVec;
typedef ROOT::VecOps::RVec<bool> bRVec;
typedef ROOT::VecOps::RVec<int> iRVec;

//------------------------------------------------------------------------
// stuctures and functions needed for trigger checking and matching
struct trig_req {
  bool pass;
  std::vector<float> pt;
  std::vector<float> eta;
  std::vector<int> id;
  std::vector<std::vector<int>> bits;  
};

bool match_trigger_object(float off_eta, float off_phi, int obj_id,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi,
    std::vector<int> bits);

bool pass_trigger(
    bool applyOfln, std::vector<TLorentzVector> off_objs, std::vector<trig_req> triggers,
    iRVec TrigObj_id, iRVec TrigObj_filterBits, fRVec TrigObj_eta, fRVec TrigObj_phi);

//------------------------------------------------------------------------
// stuctures and functions needed mainly for HHLeptonInterface
struct tau_pair {
  int index1;
  float iso1;
  float pt1;
  int index2;
  float iso2;
  float pt2;
  int isTauTauJetTrigger;
  int isQuadJetTrigger;
  int isVBFtrigger;
};

struct lepton_output {
    int pairType;
    int dau1_index;
    int dau2_index;
    int isTauTauJetTrigger;
    int isQuadJetTrigger;
    int isVBFtrigger;
    int isOS;
    int nleps;

    float dau1_eta;
    float dau1_phi;
    float dau1_iso;
    float dau1_eleMVAiso;
    int dau1_decayMode;
    int dau1_tauIdVSe;
    int dau1_tauIdVSmu;
    int dau1_tauIdVSjet;

    float dau2_eta;
    float dau2_phi;
    float dau2_iso;
    float dau2_eleMVAiso;
    int dau2_decayMode;
    int dau2_tauIdVSe;
    int dau2_tauIdVSmu;
    int dau2_tauIdVSjet;

    // Method to udpate values of lepton_output
    void update(int evt_pairType, int evt_dau1_index, int evt_dau2_index, int evt_isTauTauJetTrigger, int evt_isQuadJetTrigger, int evt_isVBFtrigger,
        int evt_isOS, int evt_nleps, float evt_dau1_eta, float evt_dau1_phi, float evt_dau1_iso, float evt_dau1_eleMVAiso,
        int evt_dau1_decayMode, int evt_dau1_tauIdVSe, int evt_dau1_tauIdVSmu, int evt_dau1_tauIdVSjet,
        float evt_dau2_eta, float evt_dau2_phi, float evt_dau2_iso, float evt_dau2_eleMVAiso,
        int evt_dau2_decayMode, int evt_dau2_tauIdVSe, int evt_dau2_tauIdVSmu, int evt_dau2_tauIdVSjet)
    {
        pairType = evt_pairType;
        dau1_index = evt_dau1_index;
        dau2_index = evt_dau2_index;
        isTauTauJetTrigger = evt_isTauTauJetTrigger;
        isQuadJetTrigger = evt_isQuadJetTrigger;
        isVBFtrigger = evt_isVBFtrigger;
        isOS = evt_isOS;
        nleps = evt_nleps;
        dau1_eta = evt_dau1_eta;
        dau1_phi = evt_dau1_phi;
        dau1_iso = evt_dau1_iso;
        dau1_eleMVAiso = evt_dau1_eleMVAiso;
        dau1_decayMode = evt_dau1_decayMode;
        dau1_tauIdVSe = evt_dau1_tauIdVSe;
        dau1_tauIdVSmu = evt_dau1_tauIdVSmu;
        dau1_tauIdVSjet = evt_dau1_tauIdVSjet;
        dau2_eta = evt_dau2_eta;
        dau2_phi = evt_dau2_phi;
        dau2_iso = evt_dau2_iso;
        dau2_eleMVAiso = evt_dau2_eleMVAiso;
        dau2_decayMode = evt_dau2_decayMode;
        dau2_tauIdVSe = evt_dau2_tauIdVSe;
        dau2_tauIdVSmu = evt_dau2_tauIdVSmu;
        dau2_tauIdVSjet = evt_dau2_tauIdVSjet;
    }
};

// pairSorting for MVA isolations: higher score --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortMVA (const tau_pair& pA, const tau_pair& pB);

// pairSorting for Raw isolations: lower iso value --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortRawIso (const tau_pair& pA, const tau_pair& pB);

// pairSorting for Lep+Tauh:
//  - leg1 (lepton): lower iso value --> more isolated
//  - leg2 (tauh)  : higher score    --> more isolated
// Sorting strategy: iso leg1 -> pT leg1 -> iso leg2 -> pT leg2
bool pairSortHybrid (const tau_pair& pA, const tau_pair& pB);

//------------------------------------------------------------------------
// stuctures and functions needed mainly for HHJetsInterface
struct jet_idx_btag {
  int idx;
  float btag;
};

struct jet_pair_mass {
  int idx1;
  int idx2;
  float inv_mass;
};

struct output {
  std::vector <float> hhbtag;
  int bjet_idx1;
  int bjet_idx2;
  int vbfjet_idx1;
  int vbfjet_idx2;
  std::vector<int> ctjet_indexes;
  std::vector<int> fwjet_indexes;
  int isBoosted;
  int fatjet_idx;
};

struct leps {
  float dau1_pt = -999.;
  float dau1_eta = -999.;
  float dau1_phi = -999.;
  float dau1_mass = -999.;
  float dau2_pt = -999.;
  float dau2_eta = -999.;
  float dau2_phi = -999.;
  float dau2_mass = -999.;
};

bool jetSort (const jet_idx_btag& jA, const jet_idx_btag& jB);

bool jetPairSort (const jet_pair_mass& jA, const jet_pair_mass& jB);

#endif // HHutils_h